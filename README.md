# pygenomics-examples

This repository provides a collection of Python scripts for
bioinformatic analysis arranged into a Python package. The scripts use
routines from packages
[_pygenomics_](https://gitlab.com/gtamazian/pygenomics) and
[_pygenomics-ext_](https://gitlab.com/gtamazian/pygenomics-ext). Each
script is provided with a command-line interface with an integrated
help message.

## Installation

_Pygenomics-examples_ can be installed using _pip_:

```
pip install git+https://gitlab.com/gtamazian/pygenomics-examples.git
```

## Running scripts

Scripts can be run by passing the package and script names to Python
using the `-m` command-line option, for example:

```
python3 -m pygenomics_examples.deduplicate_fasta input.fa output.fa
```

## Test data files

Input and output files that demonstrate how the scripts work are
located in directory `tests/data`. Names of input and output files
have suffixes `.in` and `.out`, respectively. For example, the input
file for script `get_duplicates` is named `get_duplicates.in` and the
expected output of the script is given in file `get_duplicates.out`.

## Scripts

The following table lists the package scripts and the related
bioinformatic formats.

| Script                          | Formats     | Description                                                                                                                     |
|---------------------------------|-------------|---------------------------------------------------------------------------------------------------------------------------------|
| annotate_busco_genes            | GFF3        | Annotate BUSCO-derived genes with information from [OrthoDB](https://www.orthodb.org).                                          |
| compare_fasta                   | FASTA       | Compare sequences from two FASTA files.                                                                                         |
| count_base_qualities            | FASTQ       | Count base quality values of reads from a FASTQ file.                                                                           |
| count_gap_lengths               | FASTA       | Count gaps of different lengths in sequences from a FASTA file.                                                                 |
| count_genotypes                 | VCF         | Count genotypes for samples from a VCF file.                                                                                    |
| count_nucleotides               | FASTA       | Count nucleotides for each sequences from a FASTA file.                                                                         |
| count_paired_read_lengths       | FASTQ       | Count lengths of paired-end reads.                                                                                              |
| create_vcf_from_last_maf        | MAF, VCF    | Create a VCF file of variants from a MAF file produced by [_LAST_](https://gitlab.com/mcfrith/last).                            |
| deduplicate_fasta               | FASTA       | Deduplicate sequences from a FASTA file.                                                                                        |
| estimate_genomic_divergence     | VCF         | Estimate genome-wide divergence based on SNPs.                                                                                  |
| estimate_heterozygosity         | FASTA, VCF  | Estimate heterozygosity using SNPs.                                                                                             |
| examine_illumina_reads          | FASTQ       | Examine meta-information specific to Illumina reads.                                                                            |
| extract_ncbi_coding_genes       | GFF3        | Extract GFF3 records of protein-coding genes from an [NCBI GFF3](https://www.ncbi.nlm.nih.gov/genbank/genomes_gff/) file.       |
| filter_biallelic_variants       | VCF         | Filter genomic variants with a single alternative allele.                                                                       |
| filter_strict_biallelic_snps    | VCF         | Filter biallelic SNP records with single-nucleotide REF and ALT fields.                                                         |
| filter_vcf_records_by_seq       | VCF         | Keep records from specified sequences only.                                                                                     |
| format_fasta                    | FASTA       | Reformat a FASTA file.                                                                                                          |
| get_bam_tags                    | BAM         | Get values of specified tags from a BAM file.                                                                                   |
| get_busco_duplication_regions   | custom      | Get duplicated regions in a genome assembly using its annotation by [_BUSCO_](https://busco.ezlab.org).                         |
| get_busco_metaeuk_genes         | GFF3        | Extract the gene annotation from [_BUSCO_](https://busco.ezlab.org)-[_Metaeuk_](https://github.com/soedinglab/metaeuk) results. |
| get_duplicates                  | FASTA       | Report duplicated sequence in a FASTA file.                                                                                     |
| get_gap_regions                 | BED, FASTA  | Report gaps in sequences of a FASTA file in the BED format.                                                                     |
| get_gff3_feature_regions        | BED, GFF3   | Get the BED file of GFF3 regions for features of the specified type.                                                            |
| get_iupac_fasta_vcf             | FASTA, VCF  | Get the VCF file of variants for a FASTA file with ambiguous IUPAC nucleotide symbols.                                          |
| get_juicebox_assembly           | FASTA       | Produce the FASTA file of a [Juicebox](https://www.dnazoo.org/methods) assembly.                                                |
| get_juicebox_assembly_structure | GFF3        | Convert a [Juicebox](https://www.dnazoo.org/methods) assembly file to the GFF3 format.                                          |
| get_ncbi_assembly_info          | GFF3        | Get information about assembled sequences for a genome assembly from NCBI.                                                      |
| get_sample_specific_alleles     | VCF         | Get variants with alleles specific to a selected sample.                                                                        |
| get_softmasked_regions          | BED, FASTA  | Get the BED file of soft-masked regions in sequences from a FASTA file.                                                         |
| get_vcf_ref_lengths             | VCF         | Get lengths of reference sequences from the VCF file header.                                                                    |
| liftover_juicebox_bed           | BED         | Lift over intervals from a BED file to a [Juicebox](https://www.dnazoo.org/methods) assembly.                                   |
| liftover_juicebox_gff3          | GFF3        | Lift over intervals from a GFF3 file to a [Juicebox](https://www.dnazoo.org/methods) assembly.                                  |
| parse_augustus_gff              | FASTA, GFF3 | Parse a GFF3 file of genes annotated by [_Augustus_](https://bioinf.uni-greifswald.de/augustus/).                               |
| reduce_repeatmasker_regions     | BED         | Get a reduced BED file of genomic repeat regions annotated by [_RepeatMasker_](https://repeatmasker.org).                       |
| rmout_to_bed                    | BED         | Convert a [RepeatMasker](https://repeatmasker.org) out file to the BED format.                                                  |
| run_parallel_freebayes          | FASTA, VCF  | Run [_FreeBayes_](https://github.com/freebayes/freebayes) in parallel.                                                          |
| split_fasta                     | FASTA       | Split a FASTA file into the specified number of chunks.                                                                         |
| split_into_contigs              | FASTA       | Split scaffold sequences from a FASTA file into contigs.                                                                        |
| summarize_assembly              | FASTA       | Summarize scaffolds and contigs of a genome assembly.                                                                           |
| summarize_effects               | VCF         | Summarize gene effects predicted by [_SnpEff_](https://pcingola.github.io/SnpEff/) for biallelic variants.                      |
| swap_ncbidb_gff3                | GFF3        | Swap NCBI RefSeq and GenBank sequence accessions in a GFF3 file.                                                                |
| trim_sequences                  | BED, FASTA  | Trim sequences from a FASTA file according to regions from a BED file.                                                          |
