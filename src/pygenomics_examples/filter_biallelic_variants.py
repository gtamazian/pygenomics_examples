#!/usr/bin/env python3

"""Filter genomic variants with a single alternative allele.

The script removes extra information from the variant records and
filters out variants that have 'N's in their reference alleles.

"""

import argparse
import gzip
import itertools
import sys
from typing import List, Optional, Set

from pygenomics.vcf.data.genotype import GenotypeRecord
from pygenomics.vcf.data.record import FormatField, GenotypeFields, InfoField, Qual
from pygenomics.vcf.data.record import Record as VcfRecord
from pygenomics.vcf.data.record import SampleField
from pygenomics.vcf.header import Line as VcfHeaderLine
from pygenomics.vcf.meta.metaheader import MetaHeader
from pygenomics.vcf.reader import Reader as VcfReader


def print_vcf_meta_header(meta: MetaHeader) -> None:
    """Print the VCF file meta header to standard output."""
    for k in itertools.chain(
        meta.regular,
        meta.structured,
        meta.contigs,
        meta.info_lines,
        meta.format_lines,
        meta.alt,
        meta.filter_lines,
        meta.pedigree,
        meta.meta_lines,
        meta.sample_lines,
    ):
        print(k)


class IncorrectGenotypes(Exception):
    """Indicates that a VCF record has an incorrect genotype line."""

    record: VcfRecord

    def __init__(self, record: VcfRecord):
        super().__init__(record)
        self.record = record


def strip_record(record: VcfRecord) -> VcfRecord:
    """Remove non-genotype fields from a VCF record."""
    genotypes = GenotypeFields.of_string(record.genotype_line)
    if genotypes is None:
        raise IncorrectGenotypes(record)
    if "GT" in genotypes.format_field.parts:
        if genotypes.format_field.parts[0] != "GT":
            raise IncorrectGenotypes(record)
        stripped_genotypes = str(
            GenotypeFields(
                FormatField(["GT"]),
                [SampleField([k.parts[0]]) for k in genotypes.samples],
            )
        )
    else:
        stripped_genotypes = ""
    return VcfRecord(
        record.chrom,
        record.pos,
        record.variant_id,
        record.ref,
        record.alt,
        Qual(None),
        None,
        InfoField([]),
        stripped_genotypes,
    )


def is_biallelic(
    samples: List[str], excluded_samples: Set[str], record: VcfRecord
) -> bool:
    """Does the variant record designate a biallelic locus?"""
    genotype_fields = GenotypeFields.of_string(record.genotype_line)
    if genotype_fields is None:
        raise IncorrectGenotypes(record)
    genotypes = [
        GenotypeRecord.of_string(k.parts[0][0])
        for j, k in zip(samples, genotype_fields.samples)
        if k.parts[0] and j not in excluded_samples
    ]
    filtered_genotypes = [k for k in genotypes if k is not None]
    if len(filtered_genotypes) < len(genotypes):
        raise IncorrectGenotypes(record)
    alleles = set(
        itertools.chain.from_iterable(
            k.alleles for k in filtered_genotypes if k.alleles is not None
        )
    )
    return len(alleles) <= 2


def check_reference_genotype(record: VcfRecord, index: Optional[int]) -> bool:
    """Check the reference individual genotype."""
    if index is None:
        return True
    genotype_fields = GenotypeFields.of_string(record.genotype_line)
    if genotype_fields is None:
        raise IncorrectGenotypes(record)
    ref_genotype = genotype_fields.samples[index].parts[0]
    if not ref_genotype:
        return False
    parsed_genotype = GenotypeRecord.of_string(ref_genotype[0])
    assert parsed_genotype is not None
    return parsed_genotype.alleles is not None and 0 in parsed_genotype.alleles


def filter_biallelic_variants(args: argparse.Namespace) -> None:
    """Main function of the script."""
    excluded_samples = set(args.excluded) if args.excluded is not None else set()
    with gzip.open(args.vcf_file, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        if not set.issubset(excluded_samples, set(reader.samples)):
            print(
                "Missing samples specified as the ones to be excluded: "
                + str.join(", ", set.difference(excluded_samples, set(reader.samples))),
                file=sys.stderr,
            )
            sys.exit(1)
        if args.reference is not None and args.reference not in reader.samples:
            print(
                str.format("Missing reference sample: {:s}", args.reference),
                file=sys.stderr,
            )
            sys.exit(1)
        reference_index = (
            [j for j, k in enumerate(reader.samples) if k == args.reference][0]
            if args.reference is not None
            else None
        )
        print_vcf_meta_header(reader.meta)
        print(VcfHeaderLine(reader.samples))
        for record in reader.data:
            if args.single_alt_allele and len(record.alt) > 1:
                continue
            if "N" in record.ref.bases:
                continue
            stripped_record = strip_record(record)
            if is_biallelic(
                reader.samples, excluded_samples, stripped_record
            ) and check_reference_genotype(record, reference_index):
                print(stripped_record)


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Filter biallelic variants.")
    parser.add_argument("vcf_file", help="gzip-compressed VCF file of variants")
    parser.add_argument(
        "-r",
        "--reference",
        help="individual which genotype must have at least one reference allele",
    )
    parser.add_argument(
        "-e",
        "--excluded",
        metavar="SAMPLE",
        nargs="+",
        help="samples which genotypes are allowed to contain extra alleles",
    )
    parser.add_argument(
        "-s",
        "--single-alt-allele",
        action="store_true",
        help="require a single alternative allele",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    filter_biallelic_variants(parse_args())
