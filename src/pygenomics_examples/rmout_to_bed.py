#!/usr/bin/env python3

"""Convert a RepeatMasker out file to the BED format."""

import gzip
import itertools
import sys

from pygenomics import bed
from pygenomics.strand import Strand
from pygenomics_ext import repeatmasker


def get_record_name(record: repeatmasker.Record) -> str:
    """Get the name for the BED record of a repeat."""
    if record.repeat.rep_class in {"Simple_repeat", "Low_complexity"}:
        return record.repeat.rep_name
    return (
        str.format(
            "{:s}:{:s}:{:s}",
            record.repeat.rep_class,
            record.repeat.rep_family,
            record.repeat.rep_name,
        )
        if record.repeat.rep_family is not None
        else str.format("{:s}:{:s}", record.repeat.rep_class, record.repeat.rep_name)
    )


def get_bed_record(record: repeatmasker.Record) -> bed.Record:
    """Convert a RepeatMasker out record to the BED format."""
    sequence = record.query.sequence
    start = record.query.match.start - 1
    end = record.query.match.end
    name = get_record_name(record)
    strand = Strand.MINUS if record.query.is_complement else Strand.PLUS
    return bed.Record(bed.Bed6(sequence, bed.Range(start, end), name, 0, strand))


class LineError(Exception):
    """Error while parsing a line from a RepeatMasker .out file."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def rmout_to_bed(path: str) -> None:
    """Main function of the script."""
    with gzip.open(path, "rt", encoding="utf-8") as rmout_file:
        for line in map(str.rstrip, itertools.islice(rmout_file, 3, None)):
            record = repeatmasker.Record.of_string(line)
            if record is None:
                raise LineError(line)
            print(get_bed_record(record))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 rmout_to_bed.py repeats.out.gz", file=sys.stderr)
        sys.exit(1)
    rmout_to_bed(sys.argv[1])
