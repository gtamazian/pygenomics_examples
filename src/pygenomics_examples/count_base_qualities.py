#!/usr/bin/env python3

"""Count base quality values of reads from a FASTQ file."""

import argparse
import collections
import gzip
import itertools
import operator
from typing import Counter, List, Optional

from pygenomics import fastq, phred


def count_qual_values(path: str) -> Counter[str]:
    """Count base quality values in reads from a FASTQ file."""
    with gzip.open(path, "rt") as fastq_file:
        return collections.Counter(
            itertools.chain.from_iterable(k.qual for k in fastq.read(fastq_file))
        )


def print_qual_counts(counts: Counter[str], base: int) -> None:
    """Print base quality counts."""
    print("# " + str.join("\t", ["Symbol", "Value", "Probability", "Count"]))
    for qual, count in sorted(dict.items(counts), key=operator.itemgetter(0)):
        print(
            str.format(
                "{:s}\t{:d}\t{:.2g}\t{:d}",
                qual,
                ord(qual),
                phred.of_score(ord(qual) - base),
                count,
            )
        )


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments."""
    parser = argparse.ArgumentParser(
        description="Count base quality values in reads from a FASTQ file."
    )
    parser.add_argument("in_fastq", help="FASTQ file of reads")
    parser.add_argument(
        "-s",
        "--start",
        type=int,
        default=33,
        help="starting value for quality character codes",
    )
    return parser.parse_args(args)


def count_base_qualities(args: argparse.Namespace) -> None:
    """Main function of the script."""
    print_qual_counts(count_qual_values(args.in_fastq), args.start)


if __name__ == "__main__":
    count_base_qualities(parse_args())
