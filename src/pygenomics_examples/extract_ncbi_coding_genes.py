#!/usr/bin/env python3

"""Extract GFF3 records for protein-coding genes from an NCBI GFF3 file."""

import argparse
import functools
from typing import Iterator, List, Optional, Set, Tuple

from pygenomics.cli.interval import common
from pygenomics.common import assoc_single
from pygenomics.gff import gff3


class MissingIDError(Exception):
    """Indicates the missing ID attribute."""

    record: gff3.Record

    def __init__(self, record: gff3.Record):
        super().__init__(record)
        self.record = record


def get_id(record: gff3.Record) -> str:
    """Get the record ID."""
    id_value = assoc_single(record.attributes.pairs, "ID")
    if id_value is None:
        raise MissingIDError(record)
    return id_value


def get_parent(record: gff3.Record) -> Optional[str]:
    """Get the record parent ID."""
    return assoc_single(record.attributes.pairs, "Parent")


GENE_FEATURES = {"gene", "mRNA", "CDS", "exon"}


def is_coding_gene(records: List[gff3.Record], legacy_format: bool) -> bool:
    """Does the series of GFF3 records represent a coding gene?"""
    if not records:
        return False
    if records[0].pos.feature_type != "gene":
        return False
    if legacy_format:
        feature_types = {k.pos.feature_type for k in records}
        if not set.issuperset(GENE_FEATURES, feature_types):
            return False
        pseudo = assoc_single(records[0].attributes.pairs, "pseudo")
        return pseudo is None or pseudo != "true"
    gene_biotype = assoc_single(records[0].attributes.pairs, "gene_biotype")
    return gene_biotype is not None and gene_biotype == "protein_coding"


ATTR_NAMES = {
    "gene": {"ID", "Name", "gene", "gene_biotype"},
    "mRNA": {"ID", "Parent", "Name"},
    "CDS": {"ID", "Parent", "protein_id"},
}


def filter_attributes(record: gff3.Record) -> gff3.Record:
    """Filter attributes of a GFF3 gene record."""
    assert record.pos.feature_type in {"gene", "mRNA", "CDS"}
    new_attributes = [
        (j, k)
        for j, k in record.attributes.pairs
        if j in ATTR_NAMES[record.pos.feature_type]
    ]
    if record.pos.feature_type == "gene":
        new_attributes += [("annotation", "ncbi")]
    return gff3.Record(record.pos, gff3.Attributes(new_attributes))


def filter_gene_records(records: List[gff3.Record]) -> List[gff3.Record]:
    """Filter GFF3 records that form a gene."""

    Acc = Tuple[List[gff3.Record], Set[str]]

    def process_record(acc: Acc, new: gff3.Record) -> Acc:
        """Process a new GFF3 record of a gene."""
        processed, bad_parents = acc
        if new.pos.feature_type not in {"gene", "mRNA", "CDS"}:
            set.add(bad_parents, get_id(new))
        else:
            parent = get_parent(new)
            assert parent is not None
            if parent not in bad_parents:
                list.append(processed, new)
        return (processed, bad_parents)

    if not records:
        return []

    assert records[0].pos.feature_type == "gene"
    init: Acc = ([records[0]], set())
    result, _ = functools.reduce(process_record, records[1:], init)
    return result


def iterate_grouped_records(
    records: Iterator[gff3.Record],
) -> Iterator[List[gff3.Record]]:
    """Iterate grouped records."""
    try:
        k = next(records)
    except StopIteration:
        return

    current = [k]
    for k in records:
        current_parent = get_parent(k)
        if current_parent is None:
            yield current
            current = [k]
        else:
            list.append(current, k)

    yield current


def get_coding_genes(path: str, legacy_format: bool) -> Iterator[List[gff3.Record]]:
    """Get series of GFF3 records for protein-coding genes."""
    return (
        [filter_attributes(j) for j in filter_gene_records(k)]
        for k in iterate_grouped_records(common.iterate_gff3_records(path))
        if is_coding_gene(k, legacy_format)
    )


def extract_protein_coding_genes(args: argparse.Namespace) -> None:
    """Main function of the script."""
    for gene in get_coding_genes(args.gff3_file, args.legacy):
        for record in gene:
            print(record)


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Extract gene records from an NCBI GFF3 file."
    )
    parser.add_argument("gff3_file", help="NCBI GFF3 file")
    parser.add_argument(
        "-l",
        "--legacy",
        action="store_true",
        help="input file in the NCBI legacy format",
    )
    return parser.parse_args(args)


if __name__ == "__main__":
    extract_protein_coding_genes(parse_args())
