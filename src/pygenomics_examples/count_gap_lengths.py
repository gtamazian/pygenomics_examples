#!/usr/bin/env python3

"""Count gaps of different lengths in sequences from a FASTA file."""

import collections
import itertools
import operator
import sys
from typing import Counter, List

from pygenomics import contig, fasta

from pygenomics_examples.split_into_contigs import read_sequences


def count_gaps(records: List[fasta.Record]) -> Counter[int]:
    """Get gap length counts for a nucleotide record."""
    return collections.Counter(
        k.gap_length
        for k in itertools.chain.from_iterable(
            contig.to_contigs(j.seq) for j in records
        )
    )


def print_gap_counts(counts: Counter[int]) -> None:
    """Print gap counts as a table."""
    print("# Gap length\tCount")
    for gap_length, count in sorted(dict.items(counts), key=operator.itemgetter(0)):
        print(str.format("{:d}\t{:d}", gap_length, count))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 count_gap_lengths.py sequences.fa", file=sys.stderr)
        sys.exit(1)
    print_gap_counts(count_gaps(read_sequences(sys.argv[1])))
