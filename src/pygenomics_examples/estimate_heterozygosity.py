"""Estimate heterozygosity using SNPs from a VCF file.

The script allows to specify the reference genome FASTA file for
  calculating the reference genome size without gaps. By default, the
  genome size is estimated using CONTIG meta-information lines from
  the input VCF file header.

"""

import argparse
import functools
import gzip
from enum import Enum, auto
from typing import List, Optional, Tuple

from pygenomics import fasta, nucleotide
from pygenomics.vcf.data.genotype import GenotypeRecord
from pygenomics.vcf.data.record import GenotypeFields, Record
from pygenomics.vcf.reader import Reader


class ErrorType(Enum):
    """Types for errors that may occur while processing genomic variants."""

    INCORRECT_GENOTYPE_FIELDS = auto()
    INCORRECT_GENOTYPE = auto()


class Error(Exception):
    """Error for a variant with more than one alternative allele."""

    error_type: ErrorType
    variant: Record

    def __init__(self, error_type: ErrorType, variant: Record):
        super().__init__(error_type, variant)
        self.error_type = error_type
        self.variant = variant


def is_proper_snp(variant: Record) -> bool:
    """Check the variant is an SNP with single-nucleotide alleles."""
    return len(variant.ref.bases) == 1 and all(
        k.content is not None and len(k.content) == 1 for k in variant.alt
    )


def get_genotype_heterozygosity(variant: Record) -> List[bool]:
    """Check heterozygosity of each genotype of the variant."""
    genotype_fields = GenotypeFields.of_string(variant.genotype_line)
    if genotype_fields is None:
        raise Error(ErrorType.INCORRECT_GENOTYPE_FIELDS, variant)
    genotypes = [
        GenotypeRecord.of_string(k.parts[0][0])
        if k.parts[0]
        else GenotypeRecord(None, False, True, 2)
        for k in genotype_fields.samples
    ]
    filtered_genotypes = [k for k in genotypes if k is not None]
    if len(filtered_genotypes) < len(genotypes):
        raise Error(ErrorType.INCORRECT_GENOTYPE, variant)
    return [GenotypeRecord.is_heterozygous(k) for k in filtered_genotypes]


def process_genotypes(counter: List[int], new_values: List[bool]) -> List[int]:
    """Add heterozygote counts to the current counter."""
    assert len(counter) == len(new_values)
    return [j + int(k) for j, k in zip(counter, new_values)]


def count_heterozygous_snps(reader: Reader) -> List[Tuple[str, int]]:
    """Count heterozygote SNPs from the VCF reader."""
    counts = functools.reduce(
        process_genotypes,
        (get_genotype_heterozygosity(k) for k in reader.data if is_proper_snp(k)),
        [0] * len(reader.samples),
    )
    return list(zip(reader.samples, counts))


class MissingReferenceSequenceLengths(Exception):
    """Indicates that the VCF file header contains no reference sequence lengths."""


def get_total_size_without_gaps(path: str) -> Tuple[int, int]:
    """Get the total size of sequences from a FASTA file excluding gaps."""
    with open(path, encoding="utf-8") as fasta_file:
        return functools.reduce(
            lambda x, y: (x[0] + y[0], x[1] + y[1]),
            (
                (sum(not nucleotide.is_gap(k) for k in k.seq), len(k.seq))
                for k in fasta.read(fasta_file)
            ),
            (0, 0),
        )


def estimate_heterozygosity(args: argparse.Namespace) -> None:
    """Main function of the script."""
    with gzip.open(args.vcf_file, "rt", encoding="utf-8") as vcf_file:
        reader = Reader(vcf_file)
        genome_without_gaps, total_genome_size = (
            get_total_size_without_gaps(args.reference)
            if args.reference is not None
            else (
                None,
                sum(k.length for k in reader.meta.contigs if k.length is not None),
            )
        )
        denominator = (
            genome_without_gaps
            if genome_without_gaps is not None
            else total_genome_size
        )
        if denominator == 0:
            raise MissingReferenceSequenceLengths
        sample_counts = count_heterozygous_snps(reader)
        print(
            "# "
            + str.join(
                "\t",
                [
                    "SAMPLE",
                    "NUM_HET_SNPS",
                    "GENOME_SIZE_WO_GAPS",
                    "GENOME_SIZE",
                    "HETEROZYGOSITY",
                ],
            )
        )
        for sample, num_heterozygotes in sample_counts:
            print(
                str.format(
                    "{:s}\t{:d}\t{:s}\t{:d}\t{:g}",
                    sample,
                    num_heterozygotes,
                    str.format("{:d}", genome_without_gaps)
                    if genome_without_gaps is not None
                    else "NA",
                    total_genome_size,
                    num_heterozygotes / denominator,
                )
            )


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line options of the script."""
    parser = argparse.ArgumentParser(
        description="Estimate genome-wide heterozygosity by biallelic SNPs."
    )
    parser.add_argument("vcf_file", help="VCF file of genomic variants")
    parser.add_argument("-r", "--reference", help="reference genome FASTA file")
    return parser.parse_args(args)


if __name__ == "__main__":
    estimate_heterozygosity(parse_args())
