#!/usr/bin/env python3

"""Compare sequences from two FASTA files."""

import operator
import sys
from typing import List, NamedTuple, Tuple

from pygenomics import fasta, nucleotide


def read_sequences(path: str) -> List[fasta.Record]:
    """Read unmasked nucleotide sequences from a FASTA file."""
    with open(path, "r", encoding="utf-8") as fasta_file:
        return [
            fasta.Record(k.name, k.comment, nucleotide.remove_soft_mask(k.seq))
            for k in fasta.read(fasta_file)
        ]


class SeqRecord(NamedTuple):
    """Contains the sequence name and its length in base pairs."""

    name: str
    length: int


class ComparisonResult(NamedTuple):
    """Results of comparing two sets of sequences."""

    shared: List[Tuple[SeqRecord, SeqRecord]]
    first_unique: List[SeqRecord]
    second_unique: List[SeqRecord]

    def print(self) -> None:
        """Write comparison results to the specified stream."""
        print("#Type\tFirst\tSecond\tLength")
        for first, second in self.shared:
            assert first.length == second.length
            print(
                str.format(
                    "SHARED\t{:s}\t{:s}\t{:d}", first.name, second.name, first.length
                )
            )
        for record in sorted(self.first_unique, key=operator.attrgetter("name")):
            print(str.format("FIRST\t{:s}\tNA\t{:d}", record.name, record.length))
        for record in sorted(self.second_unique, key=operator.attrgetter("name")):
            print(str.format("SECOND\tNA\t{:s}\t{:d}", record.name, record.length))


def compare_records(
    first: List[fasta.Record], second: List[fasta.Record]
) -> ComparisonResult:
    """Compare two lists of FASTA sequence records."""
    first_dict = {k.seq: k.name for k in first}
    second_set = {k.seq for k in second}
    shared = [
        (
            SeqRecord(first_dict[k.seq], len(k.seq)),
            SeqRecord(k.name, len(k.seq)),
        )
        for k in second
        if k.seq in first_dict
    ]
    first_unique = [
        SeqRecord(k.name, len(k.seq)) for k in first if k.seq not in second_set
    ]
    second_unique = [
        SeqRecord(k.name, len(k.seq)) for k in second if k.seq not in first_dict
    ]
    return ComparisonResult(shared, first_unique, second_unique)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 compare_fasta.py first.fa second.fa", file=sys.stderr)
        sys.exit(1)
    ComparisonResult.print(
        compare_records(read_sequences(sys.argv[1]), read_sequences(sys.argv[2]))
    )
