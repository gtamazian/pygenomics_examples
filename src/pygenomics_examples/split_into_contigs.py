#!/usr/bin/env python3

"""Split sequences from a FASTA file into contigs."""

import argparse
import functools
import itertools
from typing import List, Optional, Tuple

from pygenomics import contig, fasta


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""

    parser = argparse.ArgumentParser(description="Split scaffolds into contigs.")
    parser.add_argument("in_file", help="input FASTA file")
    parser.add_argument("out_file", help="output FASTA file")
    parser.add_argument(
        "--min-gap",
        default=0,
        type=int,
        help="minimal gap size for splitting into contigs (default: 0)",
    )

    return parser.parse_args(args)


def read_sequences(path: str) -> List[fasta.Record]:
    """Read nucleotide sequences from a FASTA file."""
    with open(path, "r", encoding="utf-8") as fasta_file:
        return list(fasta.read(fasta_file))


def split_record_into_contigs(record: fasta.Record, min_gap: int) -> List[fasta.Record]:
    """Split a scaffold into contigs at gaps which lengths pass the threshold."""

    Acc = Tuple[List[fasta.Record], List[contig.Contig]]

    def prepare_record(parts: List[contig.Contig], number: int) -> fasta.Record:
        """Convert a list of scaffold parts into a FASTA record."""
        return fasta.Record(
            str.format("{:s}_CONTIG_{:d}", record.name, number),
            "",
            str.rstrip(contig.of_contigs(parts), "N"),
        )

    def process_contig(acc: Acc, new_contig: contig.Contig) -> Acc:
        """Process a new contig given the current results."""
        processed, current = acc
        if new_contig.gap_length < min_gap:
            return (processed, current + [new_contig])
        return (processed + [prepare_record(current, len(processed) + 1)], [new_contig])

    contigs = contig.to_contigs(record.seq)
    if not contigs:
        return []

    init: Acc = ([], [contigs[0]])
    results, last = functools.reduce(process_contig, contigs[1:], init)
    return results + [prepare_record(last, len(results) + 1)]


def write_contigs(scaffolds: List[fasta.Record], min_gap: int, path: str) -> None:
    """Write FASTA records of contigs to the specified file."""
    with open(path, "w", encoding="utf-8") as fasta_file:
        for record in itertools.chain.from_iterable(
            split_record_into_contigs(k, min_gap) for k in scaffolds
        ):
            fasta.write(fasta_file, record)


def main(args: argparse.Namespace) -> None:
    """Main function of the script."""
    write_contigs(read_sequences(args.in_file), args.min_gap, args.out_file)


if __name__ == "__main__":
    main(parse_args())
