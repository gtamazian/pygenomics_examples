#!/usr/bin/env python3

"""Examine reads in Illumina FASTQ file."""

import collections
import gzip
import operator
import sys
from typing import NamedTuple, Optional, Tuple

from pygenomics import fastq
from pygenomics_ext.fastq import illumina


class ReadOrigin(NamedTuple):
    """Origin of an Illumina read."""

    instrument: str
    run: int
    flowcell: str
    lane: int

    def __str__(self) -> str:
        return str.format(
            "{:s}:{:d}:{:s}:{:d}",
            self.instrument,
            self.run,
            self.flowcell,
            self.lane,
        )


def get_read_origin(read: fastq.Record) -> Optional[ReadOrigin]:
    """Get read origin from its FASTQ record."""
    name = illumina.ReadName.of_string(read.name)
    return (
        ReadOrigin(name.instrument, name.run, name.flowcell, name.lane)
        if name is not None
        else None
    )


def scan_fastq_file(path: str) -> collections.Counter[Tuple[Optional[ReadOrigin], int]]:
    """Scan names of reads from a FASTQ file."""
    with gzip.open(path, "rt") as fastq_file:
        return collections.Counter(
            (get_read_origin(read), len(read.seq)) for read in fastq.read(fastq_file)
        )


def print_summary(
    counts: collections.Counter[Tuple[Optional[ReadOrigin], int]]
) -> None:
    """Print the summary of read origins."""
    print("# " + str.join("\t", ["Origin", "Read length", "Count"]))
    for (name, seq_len), count in sorted(
        dict.items(counts), key=operator.itemgetter(1), reverse=True
    ):
        print(
            str.format(
                "{}\t{:d}\t{:d}", name if name is not None else "NA", seq_len, count
            )
        )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 examine_illumina_reads.py reads.fq.gz", file=sys.stderr)
        sys.exit(1)
    print_summary(scan_fastq_file(sys.argv[1]))
