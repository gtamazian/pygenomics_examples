#!/usr/bin/env python3

"""Lift over intervals from a BED file to a Juicebox assembly."""

import argparse
from typing import Dict, List, NamedTuple, Optional, Tuple

from pygenomics import bed
from pygenomics.cli.interval import common
from pygenomics.interval import GenomicBase
from pygenomics.strand import Strand
from pygenomics_ext import juicebox

Interval = Tuple[str, int, int]


class MappedInterval(NamedTuple):
    """Pair of intervals mapped between the original and Juicebox assemblies."""

    original: Interval
    juicebox: Interval
    is_reverse: bool


def get_assembly_map(path: str) -> List[MappedInterval]:
    """Map original assembly fragments to the Juicebox assembly sequences."""
    assembly_map = juicebox.Assembly.read(path)
    original = juicebox.get_fragment_intervals(assembly_map.fragments)
    gap_number = assembly_map.fragments[-1].number
    assert gap_number == len(original)
    return [
        MappedInterval(original[number - 1], target, is_reverse)
        for target, number, is_reverse in juicebox.get_target_intervals(assembly_map)
        if number != gap_number
    ]


def invert_mapped_intervals(intervals: List[MappedInterval]) -> List[MappedInterval]:
    """Swap intervals of original and Juicebox assemblies."""
    return [MappedInterval(k.juicebox, k.original, k.is_reverse) for k in intervals]


def is_located_within(external: Interval, internal: Interval) -> bool:
    """Check that the internal interval is located within the external one."""
    return (
        external[0] == internal[0]
        and external[1] <= internal[1]
        and internal[2] <= external[2]
    )


Range = Tuple[int, int]


def map_range(base: Range, query: Range, target: Range, is_reverse: bool) -> Range:
    """Map a range in the direct orientation."""
    assert base[1] - base[0] == target[1] - target[0]
    assert base[0] <= query[0] < query[1] <= base[1]

    shift = base[1] - query[1] if is_reverse else query[0] - base[0]
    return (target[0] + shift, target[0] + shift + (query[1] - query[0]))


class Bed9Error(Exception):
    """Processing BED9 files is not supported."""


class Bed12Error(Exception):
    """Processing BED12 files is not supported."""


class AssemblyMapping:
    """Mapping from the original assembly to the Juicebox one."""

    _base: GenomicBase
    _juicebox: Dict[Interval, Tuple[Interval, bool]]

    @staticmethod
    def _adjust_interval(interval: Interval) -> Interval:
        """Adjust an interval by making it closed.

        The GenomicBase class requires closed intervals while
        intervals from a BED file are half-open.

        """
        return (interval[0], interval[1], interval[2] - 1)

    def __init__(self, intervals: List[MappedInterval]):
        self._base = GenomicBase(self._adjust_interval(k.original) for k in intervals)
        self._juicebox = {k.original: (k.juicebox, k.is_reverse) for k in intervals}

    @staticmethod
    def _adjust_back(interval: Interval) -> Interval:
        """Adjust an interval by making it half-open.

        This function is reverse to :py:func:`_adjust_interval`.

        """
        return (interval[0], interval[1], interval[2] + 1)

    def map_interval(self, interval: Interval) -> Optional[Tuple[Interval, bool]]:
        """Map an interval to the Juicebox assembly.

        The function returns `None` if the interval cannot be mapped
        as a whole.

        """
        matches = GenomicBase.find_all(self._base, interval)
        if len(matches) != 1:
            return None
        base = matches[0]
        if not is_located_within(base, self._adjust_interval(interval)):
            return None
        target, is_reverse = self._juicebox[self._adjust_back(matches[0])]
        return (
            (target[0],)
            + map_range(
                (base[1], base[2] + 1),
                (interval[1], interval[2]),
                (target[1], target[2]),
                is_reverse,
            ),
            is_reverse,
        )

    @staticmethod
    def _bed_to_range(record: bed.Record) -> Interval:
        """Adjust the start coordinate of the interval."""
        bed_range = common.bed_to_range(record)
        return (bed_range[0], bed_range[1] - 1, bed_range[2])

    def map_bed_record(self, record: bed.Record) -> Optional[bed.Record]:
        """Map a BED record to the Juicebox assembly.

        The function returns `None` if the record cannot be mapped as
        a whole.

        """

        mapping_result = self.map_interval(self._bed_to_range(record))
        if mapping_result is None:
            return None

        mapped, is_reverse = mapping_result

        if isinstance(record.record, bed.Bed12):
            raise Bed12Error()

        if isinstance(record.record, bed.Bed9):
            raise Bed9Error()

        if isinstance(record.record, bed.Bed6):
            assert record.name is not None and record.score is not None
            new_strand = (
                Strand.invert(record.record.strand)
                if is_reverse
                else record.record.strand
            )
            return bed.Record(
                bed.Bed6(
                    mapped[0],
                    bed.Range(mapped[1], mapped[2]),
                    record.name,
                    record.score,
                    new_strand,
                )
            )

        if isinstance(record.record, bed.Bed4):
            assert record.name is not None
            return bed.Record(
                bed.Bed4(mapped[0], bed.Range(mapped[1], mapped[2]), record.name)
            )

        assert isinstance(record.record, bed.Bed3)
        return bed.Record(bed.Bed3(mapped[0], bed.Range(mapped[1], mapped[2])))


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Lift over a BED file to a Juicebox assembly."
    )
    parser.add_argument("assembly", help="Juicebox .assembly file")
    parser.add_argument("bed", help="BED file to be lifted over")
    parser.add_argument(
        "-f", "--failed", help="write lift over failures to the specified file"
    )
    parser.add_argument(
        "-i", "--invert", action="store_true", help="invert the interval mapping"
    )
    return parser.parse_args(args)


def liftover_juicebox_bed(args: argparse.Namespace) -> None:
    """Main function of the script."""
    mapped_intervals = get_assembly_map(args.assembly)
    if args.invert:
        mapped_intervals = invert_mapped_intervals(mapped_intervals)
    mapping = AssemblyMapping(mapped_intervals)
    failures: List[bed.Record] = []
    for k in common.iterate_bed_records(args.bed):
        mapped_record = AssemblyMapping.map_bed_record(mapping, k)
        if mapped_record is None:
            list.append(failures, k)
        else:
            print(mapped_record)

    if args.failed is not None:
        with open(args.failed, "wt", encoding="utf-8") as failures_file:
            for k in failures:
                print(k, file=failures_file)


if __name__ == "__main__":
    liftover_juicebox_bed(parse_args())
