"""Reformat a FASTA file.

This script is useful for processing concatenation of several FASTA
files. By default, an unwrapped FASTA file is produced. A user can
specify wrapping the output file by passing the line width as a
command-line option.

"""

import argparse
from typing import IO, Callable, List, Optional

from pygenomics import fasta

Writer = Callable[[IO[str], fasta.Record], None]


def get_writer_function(wrap_width: Optional[int]) -> Writer:
    """Get the function to write FASTA records in the unwrapped or wrapped format."""
    if wrap_width is None:
        return fasta.write

    def write_wrapped(stream: IO[str], record: fasta.Record) -> None:
        """Wrapped writing function to be returned."""
        assert wrap_width is not None
        return fasta.write_wrapped(stream, record, wrap_width)

    return write_wrapped


def rewrite_fasta(in_path: str, out_path: str, writer_fn: Writer) -> None:
    """Write records from the input FASTA file to the output FASTA file."""
    with open(in_path, encoding="utf-8") as in_file, open(
        out_path, "w", encoding="utf-8"
    ) as out_file:
        for record in fasta.read(in_file):
            writer_fn(out_file, record)


def format_fasta(args: argparse.Namespace) -> None:
    """The main function of the script."""
    rewrite_fasta(args.in_fasta, args.out_fasta, get_writer_function(args.width))


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Format a FASTA file.")
    parser.add_argument("in_fasta", help="input FASTA file")
    parser.add_argument("out_fasta", help="output FASTA file")
    parser.add_argument("--width", type=int, help="line width")
    return parser.parse_args(args)


if __name__ == "__main__":
    format_fasta(parse_args())
