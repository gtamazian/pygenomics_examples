#!/usr/bin/env python3

"""Get the BED file for GFF3 regions of the specified feature type."""

import itertools
import sys
from typing import Iterable, Iterator

from pygenomics import bed
from pygenomics.gff import gff3

from pygenomics_examples.annotate_busco_genes import Gff3ReadingError


def get_gene_regions(path: str) -> Iterator[gff3.Record]:
    """Get regions of gene features from the input GFF3 file."""
    with open(path, encoding="utf-8") as gff3_file:
        for line in itertools.dropwhile(lambda x: str.startswith(x, "#"), gff3_file):
            record = gff3.Record.of_string(str.rstrip(line))
            if record is None:
                raise Gff3ReadingError(path, line)
            yield record


def print_regions(records: Iterable[gff3.Record], feature_type: str) -> None:
    """Print regions of features with the specified type in the BED format."""
    for k in records:
        if k.pos.feature_type == feature_type:
            print(
                bed.Record(
                    bed.Bed3(
                        k.pos.seqid,
                        bed.Range(k.pos.record_range.start - 1, k.pos.record_range.end),
                    )
                )
            )


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python get_gff3_feature_regions.py features.gff3 feature_type",
            file=sys.stderr,
        )
        sys.exit(1)
    print_regions(get_gene_regions(sys.argv[1]), sys.argv[2])
