#!/usr/bin/env python3

"""Trim sequences from a FASTA file according to regions from a BED file."""

import itertools
import operator
import sys
from enum import Enum, auto
from typing import List, NamedTuple, Optional, Tuple

from pygenomics import bed, fasta


class IncorrectBedLine(Exception):
    """Indicate that there is an incorrect line in a BED file."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


class RegionsErrorType(Enum):
    """Types of errors related to specifying trimmed regions."""

    MULTIPLE_REGIONS = auto()
    MISSING_START = auto()
    INTERSECTING_REGIONS = auto()
    MISSING_END = auto()


class RegionsError(Exception):
    """Error for incorrect regions to be trimmed from a sequence."""

    error_type: RegionsErrorType
    regions: List[bed.Record]

    def __init__(self, error_type: RegionsErrorType, regions: List[bed.Record]):
        super().__init__(error_type)
        self.error_type = error_type
        self.regions = regions


def read_trimmed_regions(path: str) -> List[bed.Record]:
    """Read regions to be trimmed from a FASTA file."""
    with open(path, encoding="utf-8") as bed_file:
        records = [(k, bed.Record.of_string(str.rstrip(k))) for k in bed_file]
        filtered_records = [k for _, k in records if k is not None]
        if len(filtered_records) != len(records):
            incorrect_records = [str.rstrip(j) for j, k in records if k is None]
            raise IncorrectBedLine(incorrect_records[0])
        return filtered_records


def group_trimmed_regions(regions: List[bed.Record]) -> List[List[bed.Record]]:
    """Group trimmed regios by their sequences."""
    return [
        sorted(chrom_regions, key=operator.attrgetter("start"))
        for _, chrom_regions in itertools.groupby(
            sorted(regions, key=operator.attrgetter("chrom")),
            key=operator.attrgetter("chrom"),
        )
    ]


Region = Tuple[int, int]


class TrimmedRegions(NamedTuple):
    """Regions to be trimmed from a sequence."""

    chrom: str
    left: Optional[Region]
    right: Optional[Region]


def process_grouped_regions(
    seq_regions: List[List[bed.Record]],
) -> List[TrimmedRegions]:
    """Process and check grouped regions."""

    def check_for_multiple_regions() -> None:
        """Check if more than two regions are trimmed from a sequence."""
        for k in seq_regions:
            if len(k) > 2:
                raise RegionsError(RegionsErrorType.MULTIPLE_REGIONS, k)

    def check_for_starts() -> None:
        """Check if a region from the sequence start is trimmed."""
        for k in seq_regions:
            if len(k) == 2 and 0 not in {k[0].start, k[1].start}:
                raise RegionsError(RegionsErrorType.MISSING_START, k)

    def check_for_intersections() -> None:
        """Check if trimmed regions intersect."""
        for k in seq_regions:
            if len(k) == 2 and k[0].end > k[1].start:
                raise RegionsError(RegionsErrorType.INTERSECTING_REGIONS, k)

    def convert(regions: List[bed.Record]) -> TrimmedRegions:
        """Convert a list of BED regions to the trimmed regions object."""
        if len(regions) == 1:
            if regions[0].start == 0:
                return TrimmedRegions(
                    regions[0].chrom, (regions[0].start, regions[0].end), None
                )
            return TrimmedRegions(
                regions[0].chrom, None, (regions[0].start, regions[0].end)
            )
        assert len(regions) == 2
        return TrimmedRegions(
            regions[0].chrom,
            (regions[0].start, regions[0].end),
            (regions[1].start, regions[1].end),
        )

    check_for_multiple_regions()
    check_for_starts()
    check_for_intersections()

    return [convert(k) for k in seq_regions]


def trim_record(record: fasta.Record, region: TrimmedRegions) -> fasta.Record:
    """Trim the sequence of a FASTA record."""
    if region.right is not None and region.right[1] != len(record.seq):
        raise RegionsError(
            RegionsErrorType.MISSING_END,
            [
                bed.Record(
                    bed.Bed3(region.chrom, bed.Range(region.right[0], region.right[1]))
                )
            ],
        )
    new_start = region.left[1] if region.left is not None else 0
    new_end = region.right[0] if region.right is not None else len(record.seq)
    return fasta.Record(record.name, record.comment, record.seq[new_start:new_end])


def trim_sequences(fasta_path: str, bed_path: str) -> None:
    """Trim sequences from a FASTA file using regions from a BED file."""

    trimmed_regions = {
        k.chrom: k
        for k in process_grouped_regions(
            group_trimmed_regions(read_trimmed_regions(bed_path))
        )
    }

    with open(fasta_path, "r", encoding="utf-8") as in_file:
        for record in fasta.read(in_file):
            fasta.write(
                sys.stdout,
                trim_record(record, trimmed_regions[record.name])
                if record.name in trimmed_regions
                else record,
            )


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python3 trim_sequences.py sequences.fa regions.bed", file=sys.stderr
        )
        sys.exit(1)
    trim_sequences(sys.argv[1], sys.argv[2])
