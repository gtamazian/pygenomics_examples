#!/usr/bin/env python3

"""Convert a Juicebox assembly file to the GFF3 format."""

import functools
import itertools
import sys
from typing import List, Tuple

from pygenomics.gff import base, gff3
from pygenomics.strand import Strand
from pygenomics_ext import juicebox


def process_assembly(assembly: juicebox.Assembly) -> List[gff3.Record]:
    """Produce GFF3 records for the structure of a Juicebox assembly."""

    fragments = {k.number: k for k in assembly.fragments}
    fragment_intervals = juicebox.get_fragment_intervals(assembly.fragments)
    hic_gap_number = assembly.fragments[-1].number

    def process_scaffold(
        scaffold_fragments: List[juicebox.Part], scaffold_number: int
    ) -> List[gff3.Record]:
        """Process a scaffold from the Juicebox assembly."""

        scaffold_name = str.format("HiC_scaffold_{:d}", scaffold_number)

        Acc = Tuple[List[gff3.Record], int, int]

        def process_fragment(acc: Acc, new: juicebox.Part) -> Acc:
            """Process a new fragment of an assembled scaffold."""
            processed, position, gap_count = acc
            current_fragment = fragments[new.number]
            if new.number == hic_gap_number:
                gap_count += 1
                attributes = [
                    ("ID", str.format("GAP_{:d}_{:d}", new.number, gap_count)),
                    ("Type", "gap"),
                ]
                strand = Strand.UNKNOWN
            else:
                attributes = [
                    ("ID", str.format("FRAG_{:d}", new.number)),
                    ("Type", "original"),
                    ("Scaffold", fragment_intervals[new.number - 1][0]),
                    (
                        "Range",
                        str.format(
                            "{:d}-{:d}",
                            fragment_intervals[new.number - 1][1],
                            fragment_intervals[new.number - 1][2],
                        ),
                    ),
                ] + (
                    [
                        (
                            "Extra",
                            str.join(":", (str(k) for k in current_fragment.features)),
                        )
                    ]
                    if current_fragment.features
                    else []
                )
                strand = Strand.MINUS if new.is_reverse else Strand.PLUS
            new_record = gff3.Record(
                base.Record(
                    scaffold_name,
                    None,
                    "region",
                    base.Range(position, position + current_fragment.size - 1),
                    None,
                    strand,
                    None,
                ),
                gff3.Attributes(attributes),
            )
            position += current_fragment.size
            list.append(processed, new_record)
            return (processed, position, gap_count)

        init: Acc = ([], 1, 0)
        scaffold_records, _, _ = functools.reduce(
            process_fragment, scaffold_fragments, init
        )
        return scaffold_records

    return list(
        itertools.chain.from_iterable(
            process_scaffold(k, j) for j, k in enumerate(assembly.sequences, start=1)
        )
    )


def get_juicebox_assembly_structure(path: str) -> None:
    """Main function of the script."""
    for record in process_assembly(juicebox.Assembly.read(path)):
        print(str(record))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            "Usage: python3 get_juicebox_assembly_structure.py hic.assembly",
            file=sys.stderr,
        )
        sys.exit(1)
    get_juicebox_assembly_structure(sys.argv[1])
