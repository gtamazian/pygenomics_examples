#!/usr/bin/env python3

"""Filter biallelic SNP records that have single-nucleotide REF and ALT.

This script is designed primarily for passing its output to legacy
tools that do not allow multiallelic or multinucleotide variants.

"""

import sys

from pygenomics.vcf.data import alt
from pygenomics.vcf.header import Line as VcfHeaderLine
from pygenomics.vcf.reader import Reader as VcfReader

from pygenomics_examples.filter_biallelic_variants import print_vcf_meta_header


def filter_strict_biallelic_snps() -> None:
    """Main function of the script."""
    reader = VcfReader(sys.stdin)
    print_vcf_meta_header(reader.meta)
    print(VcfHeaderLine(reader.samples))
    for record in reader.data:
        if len(record.ref.bases) == len(record.alt) == 1:
            alt_allele = record.alt[0]
            if alt_allele.value_type is alt.FieldType.BASE_PAIRS:
                assert alt_allele.content is not None
                print(record)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        print(
            "Usage: zcat variants.vcf.gz | python3 filter_strict_biallelic_snps.py",
            file=sys.stderr,
        )
        sys.exit(1)
    filter_strict_biallelic_snps()
