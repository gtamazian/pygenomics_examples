#!/usr/bin/env python3

"""Produce the FASTA file of a Juicebox assembly."""

import sys
from typing import Dict, List

from pygenomics import fasta
from pygenomics.nucleotide import rev_comp
from pygenomics_ext import juicebox


def read_sequences(path: str) -> Dict[str, str]:
    """Read sequences from a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        return {k.name: k.seq for k in fasta.read(fasta_file)}


def get_fragments(
    sequences: Dict[str, str], fragments: List[juicebox.Fragment]
) -> Dict[int, str]:
    """Get the dictionary which keys are fragment IDs and values are their sequences."""
    result = {
        j.number: sequences[seq][start:end]
        for j, (seq, start, end) in zip(
            fragments, juicebox.get_fragment_intervals(fragments[:-1])
        )
    }
    result[fragments[-1].number] = "N" * fragments[-1].size
    return result


def produce_assembly(
    sequences: Dict[str, str], assembly: juicebox.Assembly
) -> List[fasta.Record]:
    """Assembly sequences specified by a Juicebox assembly."""
    fragments = get_fragments(sequences, assembly.fragments)
    return [
        fasta.Record(
            str.format("HiC_scaffold_{:d}", j),
            "",
            str.join(
                "",
                (
                    rev_comp(fragments[k.number])
                    if k.is_reverse
                    else fragments[k.number]
                    for k in sequence
                ),
            ),
        )
        for j, sequence in enumerate(assembly.sequences, start=1)
    ]


def get_juicebox_assembly(fasta_path: str, assembly_path: str) -> None:
    """Main function of the script."""
    for k in produce_assembly(
        read_sequences(fasta_path), juicebox.Assembly.read(assembly_path)
    ):
        fasta.write(sys.stdout, k)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python3 get_juicebox_assembly.py reference.fasta juicebox.assembly",
            file=sys.stderr,
        )
        sys.exit(1)
    get_juicebox_assembly(sys.argv[1], sys.argv[2])
