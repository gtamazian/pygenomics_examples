#!/usr/bin/env python3

"""Get soft-masked regions in sequences of a FASTA file."""

import sys
from typing import List, Tuple

from pygenomics import bed, fasta

Interval = Tuple[int, int]


def get_regions(sequence: str) -> List[Interval]:
    """Get soft-masked regions in the sequence."""

    Acc = Tuple[List[Interval], int, bool]

    def find_masked(start: int) -> int:
        """Find a masked character position starting from the specified one."""
        for k in range(start, len(sequence)):
            if str.islower(sequence[k]):
                return k
        return -1

    def find_unmasked(start: int) -> int:
        """Find an unmasked character position starting from the specified one."""
        for k in range(start, len(sequence)):
            if str.isupper(sequence[k]):
                return k
        return -1

    def process_rest(acc: Acc) -> Acc:
        """Process the remaining part of the sequence."""
        regions, position, is_masked = acc
        if is_masked:
            next_index = find_unmasked(position)
            if next_index == -1:
                # we have reached the sequence end
                list.append(regions, (position, len(sequence)))
                position = len(sequence)
            else:
                list.append(regions, (position, next_index))
                position = next_index
            is_masked = False
        else:
            next_index = find_masked(position)
            if next_index == -1:
                # we have reached the sequence end
                position = len(sequence)
            else:
                position = next_index
            is_masked = True
        return (regions, position, is_masked)

    if not sequence:
        return []

    if len(sequence) == 1:
        return [(0, 1)] if str.islower(sequence[0]) else []

    acc: Acc = ([], 0, True) if str.islower(sequence[0]) else ([], 1, False)
    while acc[1] < len(sequence):
        acc = process_rest(acc)

    return acc[0]


def get_softmasked_regions(path: str) -> None:
    """Print soft-masked regions from a sequence in the BED format."""
    with open(path, encoding="utf-8") as fasta_file:
        for record in fasta.read(fasta_file):
            for start, end in get_regions(record.seq):
                print(bed.Record(bed.Bed3(record.name, bed.Range(start, end))))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 get_softmasked_regions.py sequences.fa", file=sys.stderr)
        sys.exit(1)
    get_softmasked_regions(sys.argv[1])
