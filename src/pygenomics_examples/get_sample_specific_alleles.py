#!/usr/bin/env python3

"""Get variants with alleles specific to a selected sample."""

import argparse
import functools
import gzip
import itertools
import sys
from typing import List, Optional

from pygenomics.vcf.data.genotype import GenotypeRecord
from pygenomics.vcf.data.record import GenotypeFields
from pygenomics.vcf.data.record import Record as VcfRecord
from pygenomics.vcf.header import Line as VcfHeaderLine
from pygenomics.vcf.reader import Reader as VcfReader

from pygenomics_examples.filter_biallelic_variants import (
    IncorrectGenotypes,
    print_vcf_meta_header,
)


def get_alleles(record: VcfRecord) -> List[List[int]]:
    """Are the selected sample alleles specific in the variant?"""
    genotype_fields = GenotypeFields.of_string(record.genotype_line)
    if genotype_fields is None:
        raise IncorrectGenotypes(record)
    if genotype_fields.format_field.parts[0] != "GT":
        return []
    genotypes = [
        GenotypeRecord.of_string(k.parts[0][0])
        for k in genotype_fields.samples
        if k.parts[0]
    ]
    filtered_genotypes = [k for k in genotypes if k is not None]
    if len(filtered_genotypes) < len(genotypes):
        raise IncorrectGenotypes(record)
    return [k.alleles for k in filtered_genotypes if k.alleles is not None]


def are_alleles_specific(sample_index: int, alleles: List[List[int]]) -> bool:
    """Check if at least one alleles of the selected sample is specific."""
    return bool(
        set.difference(
            set(alleles[sample_index]),
            set(
                itertools.chain.from_iterable(
                    k for j, k in enumerate(alleles) if j != sample_index
                )
            ),
        )
    )


def are_both_alleles_specific(sample_index: int, alleles: List[List[int]]) -> bool:
    """Check if both alleles of the selected sample are specific."""
    return set.isdisjoint(
        set(alleles[sample_index]),
        set(
            itertools.chain.from_iterable(
                k for j, k in enumerate(alleles) if j != sample_index
            )
        ),
    )


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Get variants with sample-specific alleles."
    )
    parser.add_argument("vcf_file", help="VCF file of genomic variants")
    parser.add_argument("sample", help="name of the selected sample")
    parser.add_argument(
        "-b",
        "--both",
        action="store_true",
        help="both alleles of the selected sample must be specific",
    )
    return parser.parse_args(args)


def get_sample_specific_alleles(args: argparse.Namespace) -> None:
    """Main function of the script."""
    with gzip.open(args.vcf_file, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        sample_index = [j for j, k in enumerate(reader.samples) if k == args.sample]
        if not sample_index:
            print("The specified sample is missing from the VCF file.", file=sys.stderr)
            sys.exit(1)
        print_vcf_meta_header(reader.meta)
        print(VcfHeaderLine(reader.samples))
        check_fn = functools.partial(
            are_both_alleles_specific if args.both else are_alleles_specific,
            sample_index[0],
        )
        for record in reader.data:
            if check_fn(get_alleles(record)):
                print(record)


if __name__ == "__main__":
    get_sample_specific_alleles(parse_args())
