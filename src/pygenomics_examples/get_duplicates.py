#!/usr/bin/env python3

"""Get duplicate sequences in a FASTA file."""

import argparse
import itertools
import operator
from typing import List

from pygenomics import fasta, nucleotide

from pygenomics_examples.compare_fasta import read_sequences


def normalize_sequence(record: fasta.Record) -> fasta.Record:
    """Normalize the sequence of a FASTA record."""
    rev_comp_seq = nucleotide.rev_comp(record.seq)
    if rev_comp_seq < record.seq:
        return fasta.Record(record.name, record.comment, rev_comp_seq)
    return record


def arrange_records(
    records: List[fasta.Record], with_rc: bool
) -> List[List[fasta.Record]]:
    """Sort and group records by their sequences."""
    return [
        list(k)
        for _, k in itertools.groupby(
            sorted(
                (normalize_sequence(k) for k in records) if with_rc else records,
                key=operator.attrgetter("seq"),
            ),
            key=operator.attrgetter("seq"),
        )
    ]


def report_duplicates(arranged_records: List[List[fasta.Record]]) -> None:
    """Print names of duplicate sequence records."""
    print("#Group\tSequence\tLength")
    for k, group in enumerate(filter(lambda x: len(x) > 1, arranged_records), start=1):
        for name in sorted(record.name for record in group):
            print(str.format("GROUP_{:d}\t{:s}\t{:d}", k, name, len(group[0].seq)))


def main(args: argparse.Namespace) -> None:
    """Main function of the script."""
    report_duplicates(arrange_records(read_sequences(args.fasta), args.with_rc))


def parse_args() -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Report duplicate sequences in a FASTA file."
    )
    parser.add_argument("fasta", help="FASTA file")
    parser.add_argument(
        "--with-rc",
        action="store_true",
        help="Consider sequences equal to their reverse complements",
    )
    return parser.parse_args()


if __name__ == "__main__":
    main(parse_args())
