#!/usr/bin/env python3

"""Run FreeBayes in parallel for calling variants in a genome assembly."""

import argparse
import concurrent.futures
import functools
import itertools
import os
import subprocess
from typing import Callable, Iterator, List, NamedTuple, Optional, Set, Tuple

from pygenomics import contig, fasta

Range = Tuple[int, int]


class IncorrectQualValue(Exception):
    """An incorrect QUAL value was parsed."""

    line: bytes

    def __init__(self, line: bytes):
        super().__init__(line)
        self.line = line


class DuplicateRegionOptions(Exception):
    """The region option is specified in parameters passed by a user."""

    params: List[str]

    def __init__(self, params: List[str]):
        super().__init__(params)
        self.params = params


class DuplicateReferenceOptions(Exception):
    """The reference option is specified in parameters passed by a user."""

    params: List[str]

    def __init__(self, params: List[str]):
        super().__init__(params)
        self.params = params


def split_by_gaps(sequence: str, min_gap_size: int) -> List[Range]:
    """Split the sequence into regions by gaps of the specified size or greater."""

    Acc = Tuple[List[Range], int, int]

    def process_contig(acc: Acc, new_contig: contig.Contig) -> Acc:
        """Process the next contig in the sequence."""
        processed, chunk_start, current_position = acc
        if new_contig.gap_length < min_gap_size:
            return (
                processed,
                chunk_start,
                current_position + len(new_contig.seq) + new_contig.gap_length,
            )
        list.append(processed, (chunk_start, current_position + len(new_contig.seq)))
        new_chunk_start = current_position + len(new_contig.seq) + new_contig.gap_length
        return (processed, new_chunk_start, new_chunk_start)

    seq_contigs = contig.to_contigs(sequence)
    if (
        seq_contigs
        and not seq_contigs[0].seq
        and seq_contigs[0].gap_length >= min_gap_size
    ):
        offset = seq_contigs[0].gap_length
        seq_contigs = seq_contigs[1:]
    else:
        offset = 0
    init: Acc = ([], offset, offset)
    chunks, last_start, last_pos = functools.reduce(process_contig, seq_contigs, init)
    if last_start < last_pos:
        list.append(chunks, (last_start, last_pos))
    return chunks


class Region(NamedTuple):
    """Region for calling variants in."""

    seq: str
    coord: Range

    def __str__(self) -> str:
        return str.format("{:s}:{:d}-{:d}", self.seq, self.coord[0], self.coord[1])


def get_genotyping_regions(fasta_path: str, min_gap_size: int) -> List[Region]:
    """Get regions to be passed to FreeBayes for running in parallel."""
    with open(fasta_path, encoding="utf-8") as fasta_file:
        return [
            Region(record.name, k)
            for record in fasta.read(fasta_file)
            for k in split_by_gaps(record.seq, min_gap_size)
        ]


def is_vcf_header(line: bytes) -> bool:
    """Does the line belong to the VCF header?"""
    return bytes.startswith(line, b"#")


def get_qual(line: bytes) -> float:
    """Get the variant quality value (QUAL) from a FreeBayes output line.

    The function must get a proper VCF variant line with the QUAL
      value present, otherwise exception
      :py:class:`IncorrectQualValue` is raised.

    """
    if not is_vcf_header(line):
        parts = bytes.split(line, b"\t", 6)
        if len(parts) == 7:
            try:
                return float(parts[5])
            except ValueError as float_conversion_error:
                raise IncorrectQualValue(line) from float_conversion_error
    raise IncorrectQualValue(line)


def filter_output_by_qual(
    output: bytes, min_qual: float, keep_header: bool
) -> List[bytes]:
    """Filter variants produced by FreeBayes by their QUAL values.

    The function keeps the VCF header in its output.

    """

    def check_line(line: bytes) -> bool:
        """Check if the line is filtered."""
        if is_vcf_header(line):
            return keep_header
        return get_qual(line) >= min_qual

    return [k for k in bytes.splitlines(output) if check_line(k)]


def launch_freebayes(
    params: List[str], min_qual: float, keep_header: bool
) -> List[bytes]:
    """Launch FreeBayes with specified parameters and return the filtered results."""
    run = subprocess.run(
        ["freebayes"] + params,
        stderr=subprocess.DEVNULL,
        stdout=subprocess.PIPE,
        check=True,
    )
    return filter_output_by_qual(run.stdout, min_qual, keep_header)


Job = Callable[[Tuple[int, Region]], List[bytes]]


def create_job(ref_path: str, params: List[str], min_qual: float) -> Job:
    """Create a job that launches FreeBayes in the specified region."""

    user_region_options = [k for k in params if k in {"-r", "--region"}]
    if user_region_options:
        raise DuplicateRegionOptions(params)

    user_reference_options = [k for k in params if k in {"-f", "--fasta-reference"}]
    if user_reference_options:
        raise DuplicateReferenceOptions(params)

    def job_fn(task: Tuple[int, Region]) -> List[bytes]:
        """Function to be returned."""
        chunk_number, region = task
        return launch_freebayes(
            [
                "-f",
                ref_path,
                "-r",
                str.format(
                    "{:s}:{:d}-{:d}", region.seq, region.coord[0], region.coord[1]
                ),
            ]
            + params,
            min_qual,
            chunk_number == 0,
        )

    return job_fn


def launch_workers(
    ref_path: str,
    min_gap_size: int,
    params: List[str],
    min_qual: float,
    num_workers: int,
    excluded_scaffolds: Set[str],
) -> List[List[bytes]]:
    """Launch FreeBayes workers in parallel."""
    # pylint: disable=too-many-arguments

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        tasks = executor.map(
            create_job(ref_path, params, min_qual),
            enumerate(
                (
                    k
                    for k in get_genotyping_regions(ref_path, min_gap_size)
                    if k.seq not in excluded_scaffolds
                )
            ),
        )
        return list(tasks)


def write_output(out_path: str, task_output: List[List[bytes]]) -> None:
    """Write output from FreeBayes jobs to the specified file."""
    if not task_output:
        return

    line_sep = str.encode(os.linesep)

    with open(out_path, "wb") as vcf_file:

        def write_lines(lines: Iterator[bytes]) -> None:
            """Helper function that writes lines to the output VCF file."""
            vcf_file.writelines(k + line_sep for k in lines)

        # write the VCF header without the command meta-information line
        write_lines(
            k
            for k in itertools.takewhile(is_vcf_header, task_output[0])
            if not bytes.startswith(k, b"##commandline")
        )

        for chunk in task_output:
            write_lines(itertools.dropwhile(lambda x: bytes.startswith(x, b"#"), chunk))


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments for launching FreeBayes in parallel."""
    parser = argparse.ArgumentParser(description="Launch FreeBayes in parallel.")
    parser.add_argument("ref_fasta", help="reference genome FASTA file")
    parser.add_argument(
        "min_gap_size",
        type=int,
        help="minimal size for gaps to split the reference genome at",
    )
    parser.add_argument("params", help="parameters passed to FreeBayes")
    parser.add_argument("min_qual", type=float, help="minimal QUAL for output variants")
    parser.add_argument("num_workers", type=int, help="number of parallel workers")
    parser.add_argument("output_file", help="output VCF file name")
    parser.add_argument(
        "--excluded-scaffolds", help="comma-separated list of excluded scaffolds"
    )
    return parser.parse_args(args)


def run_parallel_freebayes(args: argparse.Namespace) -> None:
    """The main function of the script."""
    write_output(
        args.output_file,
        launch_workers(
            args.ref_fasta,
            args.min_gap_size,
            str.split(args.params),
            args.min_qual,
            args.num_workers,
            set(
                str.split(args.excluded_scaffolds, ",")
                if args.excluded_scaffolds is not None
                else []
            ),
        ),
    )


if __name__ == "__main__":
    run_parallel_freebayes(parse_args())
