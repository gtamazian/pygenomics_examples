"""Count genotypes for samples from a VCF file."""

import collections
import itertools
import sys
from typing import Counter, List, Tuple

from pygenomics.vcf.data import record
from pygenomics.vcf.reader import Reader


class IncorrectGenotype(Exception):
    """Incorrect genotype fields of a variant record."""

    genotype_line: str

    def __init__(self, genotype_line: str):
        super().__init__(genotype_line)
        self.genotype_line = genotype_line


def extract_genotypes(variant: record.Record) -> List[str]:
    """Extract genotypes from a variant record."""
    parsed_genotype = record.GenotypeFields.of_string(variant.genotype_line)
    if parsed_genotype is None:
        raise IncorrectGenotype(variant.genotype_line)
    return [k.parts[0][0] if k.parts[0] else "MISSING" for k in parsed_genotype.samples]


def print_genotype_counts(samples: List[str], counts: Counter[Tuple[str, str]]) -> None:
    """Print the genotype count table."""
    genotypes = sorted({k for _, k in counts.keys()})
    print("Genotype\t" + str.join("\t", samples))
    for k in genotypes:
        print(
            k
            + "\t"
            + str.join("\t", (str.format("{:d}", counts[(j, k)]) for j in samples))
        )


def count_genotypes() -> None:
    """Get genotype counts for a VCF file from standard input."""
    reader = Reader(sys.stdin)
    counts = collections.Counter(
        itertools.chain.from_iterable(
            zip(reader.samples, extract_genotypes(k)) for k in reader.data
        )
    )
    print_genotype_counts(reader.samples, counts)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        print(
            "Usage: zcat variants.vcf.gz | python3 count_genotypes.py", file=sys.stderr
        )
        sys.exit(1)
    count_genotypes()
