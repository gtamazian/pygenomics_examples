"""Count lengths of paired-end reads."""

import gzip
import operator
import sys
from collections import Counter
from typing import Tuple

from pygenomics import fastq


class UnpairedReads(Exception):
    """Indicates that reads in the pair have different names."""

    fwd_name: str
    rev_name: str

    def __init__(self, fwd_name: str, rev_name: str):
        self.fwd_name = fwd_name
        self.rev_name = rev_name


def get_read_lengths(fwd_read: fastq.Record, rev_read: fastq.Record) -> Tuple[int, int]:
    """Get sequence lengths for a pair of reads."""
    if str.split(fwd_read.name, None, 1)[0] != str.split(rev_read.name, None, 1)[0]:
        raise UnpairedReads(fwd_read.name, rev_read.name)
    return (len(fwd_read.seq), len(rev_read.seq))


def count_paired_read_lengths(fwd_path: str, rev_path: str) -> None:
    """Count lengths of paired-end reads."""
    with gzip.open(fwd_path, "rt") as fwd_file, gzip.open(rev_path, "rt") as rev_file:
        counts = Counter(
            get_read_lengths(fwd, rev)
            for fwd, rev in zip(fastq.read(fwd_file), fastq.read(rev_file))
        )
    print("FWD_LEN\tREV_LEN\tCOUNT")
    for (fwd_len, rev_len), count in sorted(
        dict.items(counts), key=operator.itemgetter(1), reverse=True
    ):
        print(str.format("{:d}\t{:d}\t{:d}", fwd_len, rev_len, count))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: count_paired_read_lengths fwd.fq.gz rev.fq.gz > counts",
            file=sys.stderr,
        )
        sys.exit(1)
    try:
        count_paired_read_lengths(sys.argv[1], sys.argv[2])
    except UnpairedReads as error:
        print(
            str.format(
                "Error: unpaired reads {:s} and {:s} identified.",
                error.fwd_name,
                error.rev_name,
            ),
            file=sys.stderr,
        )
        sys.exit(2)
