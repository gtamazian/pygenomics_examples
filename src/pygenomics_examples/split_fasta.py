#!/usr/bin/env python3

"""Split a FASTA file into the specified number of chunks."""

import argparse
import itertools
from typing import List, Optional

from pygenomics import fasta


def read_sequences(path: str) -> List[fasta.Record]:
    """Read sequences from a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        return list(fasta.read(fasta_file))


def group_records(
    records: List[fasta.Record], num_groups: int
) -> List[List[fasta.Record]]:
    """Arrange records from a FASTA file into groups."""

    def total_length(seq_records: List[fasta.Record]) -> int:
        """Get the total length of sequences."""
        return sum(len(k.seq) for k in seq_records)

    if not records:
        return []

    result: List[List[fasta.Record]] = [[records[0]]]
    remaining_length = total_length(records[1:])
    for k, current in enumerate(records[1:], start=1):
        result_groups = len(result)
        if result_groups < num_groups and total_length(result[-1]) + len(
            current.seq
        ) > (remaining_length // (num_groups - len(result) + 1)):
            list.append(result, [current])
        else:
            list.append(result[-1], current)
        remaining_length -= len(current.seq)

    if len(result) > num_groups:
        last_group = list(itertools.chain.from_iterable(result[num_groups - 1 :]))
        result[num_groups - 1] = last_group

    assert len(result) == num_groups
    return result


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Split a FASTA file into the specified number of chunks."
    )
    parser.add_argument("fasta", help="FASTA file to be split")
    parser.add_argument("num_chunks", type=int, help="the number of chunks")
    parser.add_argument("out_prefix", help="prefix of output file names")
    return parser.parse_args(args)


def split_fasta(args: argparse.Namespace) -> None:
    """Main function of the script."""
    for j, group in enumerate(
        group_records(read_sequences(args.fasta), args.num_chunks), start=1
    ):
        out_path = str.format("{:s}_{:d}.fa", args.out_prefix, j)
        with open(out_path, "w", encoding="utf-8") as out_file:
            for k in group:
                fasta.write(out_file, k)


if __name__ == "__main__":
    split_fasta(parse_args())
