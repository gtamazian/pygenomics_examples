"""Create a VCF file of variants from a LAST MAF file."""

import argparse
import functools
from typing import Dict, Iterator, List, Optional, Set, Tuple

from pygenomics import fasta
from pygenomics.vcf import datatypes, header
from pygenomics.vcf.data import alt, record
from pygenomics.vcf.meta import contig, infoformat, regular, valuenumber
from pygenomics_ext.last import maf

Difference = Tuple[int, str, str, Set[str]]


def enumerate_target_bases(
    alignment_iterator: Iterator[Tuple[str, str]]
) -> Iterator[Tuple[int, Tuple[str, str]]]:
    """Enumerate base pairs in the target sequence."""
    k = -1
    for target_base, query_base in alignment_iterator:
        if target_base != "-":
            k += 1
        yield (k, (target_base, query_base))


def get_differences(alignment: maf.Record) -> Tuple[str, List[Difference]]:
    """Get differences between target and query sequences from the alignment."""
    return (
        alignment.target.name,
        [
            (alignment.target.start + k, x, y, {alignment.query.name})
            for k, (x, y) in enumerate_target_bases(
                zip(
                    str.upper(alignment.target.aln_seq),
                    str.upper(alignment.query.aln_seq),
                )
            )
            if x != y
        ],
    )


def is_gap(sequence: str) -> bool:
    """Does the sequence represent a pure gap?"""
    return all(k == "-" for k in sequence)


def remove_gaps(aligned_sequence: str) -> str:
    """Remove gap characters from an aligned sequence."""
    assert not all(k == "-" for k in aligned_sequence)
    return str.join("", (k for k in aligned_sequence if k != "-"))


def remove_gaps_from_difference(difference: Difference) -> Difference:
    """Remove gaps from sequences in the difference object."""
    pos, target, query, query_names = difference
    return (pos, remove_gaps(target), remove_gaps(query), query_names)


def process_indels(ref_seq: str, difference: Difference) -> Difference:
    """Process indels by removing gaps and adding prior base pairs if necessary."""
    pos, target, query, query_names = difference
    if is_gap(target) or is_gap(query):
        result = (
            (pos - 1, ref_seq[pos - 1] + target, ref_seq[pos - 1] + query, query_names)
            if pos > 0
            else (pos, target + ref_seq[0], query + ref_seq[0], query_names)
        )
    else:
        result = difference
    return remove_gaps_from_difference(result)


def merge_adjacent_differences(differences: List[Difference]) -> List[Difference]:
    """Merge different nucleotides located next to each other."""
    if not differences:
        return []

    Acc = Tuple[List[Difference], Difference]

    def are_adjacent(left: Difference, right: Difference) -> bool:
        """Are two differences adjacent to each other on the target sequence?"""
        left_pos, left_target, _, _ = left
        right_pos, _, _, _ = right

        assert left_pos <= right_pos
        return right_pos - left_pos == sum(k != "-" for k in left_target)

    def process_difference(acc: Acc, new: Difference) -> Acc:
        """Process a new difference."""
        processed, current = acc
        if are_adjacent(current, new):
            return (
                processed,
                (
                    current[0],
                    current[1] + new[1],
                    current[2] + new[2],
                    set.union(current[3], new[3]),
                ),
            )
        return (processed + [current], new)

    init: Acc = ([], differences[0])
    merged, last = functools.reduce(process_difference, differences[1:], init)
    return merged + [last]


def get_vcf_variant_records(
    ref_name: str, ref_seq: str, differences: List[Difference]
) -> List[record.Record]:
    """Get variant records in the VCF format for the sequence differences."""
    return [
        record.Record(
            record.Chrom(ref_name, False),
            record.Pos(pos + 1),
            record.IdField([]),
            record.RefField(ref_value),
            [alt.Field(alt.FieldType.BASE_PAIRS, alt_value)],
            record.Qual(None),
            None,
            record.InfoField([("QUERY", sorted(query_names))]),
            "",
        )
        for pos, ref_value, alt_value, query_names in (
            process_indels(ref_seq, k) for k in merge_adjacent_differences(differences)
        )
    ]


def print_vcf_header(path: str, sequences: Dict[str, str]) -> None:
    """Print meta-information and header lines for the output VCF file."""
    print(regular.RegularLine("fileformat", "VCFv4.3"))
    print(regular.RegularLine("reference", str.format("file://{:s}", path)))
    print(
        infoformat.InfoFormatLine(
            infoformat.LineKind.INFO,
            "QUERY",
            valuenumber.Number(valuenumber.SpecialNumber.UNKNOWN),
            datatypes.DataType.STRING,
            "Query sequences",
            [],
        )
    )
    for name, seq in dict.items(sequences):
        print(contig.ContigLine(name, len(seq), []))
    print(header.Line([]))


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Create a VCF file from LAST alignments in the MAF format."
    )
    parser.add_argument("fasta", help="FASTA file of target (reference) sequences.")
    parser.add_argument("maf", help="MAF file of alignments by LAST.")
    return parser.parse_args(args)


def merge_similar_variants(variants: List[record.Record]) -> List[record.Record]:
    """Merge variants with same CHROM, POS, REF and ALT from different query sequences."""

    def merge_loci(left: record.Record, right: record.Record) -> record.Record:
        """Merge a pair of variants."""

        return record.Record(
            left.chrom,
            left.pos,
            left.variant_id,
            left.ref,
            left.alt,
            left.qual,
            left.filter_field,
            record.InfoField(
                [
                    (
                        "QUERY",
                        sorted(
                            set.union(
                                set(left.info_field.parts[0][1]),
                                set(right.info_field.parts[0][1]),
                            )
                        ),
                    )
                ]
            ),
            "",
        )

    Acc = List[record.Record]

    def is_same_locus(left: record.Record, right: record.Record) -> bool:
        """Do the variants share the locus?"""
        return (
            left.chrom == right.chrom
            and left.pos == right.pos
            and left.ref == right.ref
            and left.alt == right.alt
        )

    def process_variant(acc: Acc, new: record.Record) -> Acc:
        """Compare a variant record to the previous one."""
        if is_same_locus(acc[-1], new):
            return acc[:-1] + [merge_loci(acc[-1], new)]
        return acc + [new]

    if not variants:
        return []

    sorted_variants = sorted(variants, key=lambda x: (x.chrom, x.pos, x.ref, x.alt))
    init: List[record.Record] = [sorted_variants[0]]
    return functools.reduce(process_variant, sorted_variants[1:], init)


def create_vcf_from_last_maf(args: argparse.Namespace) -> None:
    """Main function of the script."""
    with open(args.fasta, encoding="utf-8") as fasta_file:
        sequences = {k.name: k.seq for k in fasta.read(fasta_file)}

    print_vcf_header(args.fasta, sequences)
    with open(args.maf, encoding="utf-8") as maf_file:
        variants: List[record.Record] = []
        for alignment in maf.read(maf_file):
            target_seq, differences = get_differences(alignment)
            for diff_variant in get_vcf_variant_records(
                target_seq,
                sequences[target_seq],
                merge_adjacent_differences(differences),
            ):
                list.append(variants, diff_variant)
    for k in merge_similar_variants(variants):
        print(k)


if __name__ == "__main__":
    create_vcf_from_last_maf(parse_args())
