#!/usr/bin/env python3

"""Get information about assembled sequences for a genome assembly from NCBI."""

import argparse
import csv
import functools
import gzip
import sys
from typing import List, NamedTuple, Optional, Set, Tuple

from pygenomics import contig, fasta
from pygenomics.common import assoc_single
from pygenomics.gff import gff3
from pygenomics_ext.ncbi import assemblyreport

Region = Tuple[int, int]


class SequenceInfo(NamedTuple):
    """Assembled sequence information."""

    accession: str
    seq_type: str
    chromosome: str
    length: int


class Gff3LineError(Exception):
    """Indicates a GFF3 record line that could not be parsed."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def get_sequence_roles(path: str) -> List[Tuple[str, str]]:
    """Get roles of assembly sequences."""
    records = assemblyreport.read(path)
    genbank_roles = [
        (k.genbank_accn, str(k.seq_role)) for k in records if k.genbank_accn is not None
    ]
    refseq_roles = [
        (k.refseq_accn, str(k.seq_role)) for k in records if k.refseq_accn is not None
    ]
    return genbank_roles + refseq_roles


def get_sequence_info(gff3_path: str, report_path: str) -> List[SequenceInfo]:
    """Read sequence information from an NCBI GFF3 file."""
    sequence_roles = dict(get_sequence_roles(report_path))
    result: List[SequenceInfo] = []
    with gzip.open(gff3_path, "rt", encoding="utf-8") as gff3_file:
        for line in gff3_file:
            if str.startswith(line, "#"):
                continue
            record = gff3.Record.of_string(str.rstrip(line))
            if record is None:
                raise Gff3LineError(line)
            if record.pos.feature_type == "region" and record.pos.start == 1:
                accession = record.pos.seqid
                length = record.pos.end
                seq_type = sequence_roles[accession]
                genome = assoc_single(record.attributes.pairs, "genome")
                if genome is not None and genome == "mitochondrion":
                    continue
                chromosome = assoc_single(record.attributes.pairs, "chromosome")
                list.append(
                    result,
                    SequenceInfo(
                        accession,
                        seq_type,
                        "NA" if chromosome is None else chromosome,
                        int(length),
                    ),
                )
    return result


Centromere = Tuple[str, int, int]


def get_centromere_info(path: str) -> List[Centromere]:
    """Read centromere information from an NCBI GFF3 file."""
    result: List[Centromere] = []
    with gzip.open(path, "rt", encoding="utf-8") as gff3_file:
        for line in gff3_file:
            if str.startswith(line, "#"):
                continue
            record = gff3.Record.of_string(str.rstrip(line))
            if record is None:
                raise Gff3LineError(line)
            if record.pos.feature_type == "centromere":
                list.append(
                    result, (record.pos.seqid, record.pos.start, record.pos.end)
                )
    return result


def get_centromeres_from_fasta(
    path: str, sequences: Set[str], gap_sizes: Set[int]
) -> List[Centromere]:
    """Get centromere regions from sequences of a FASTA file."""
    result: List[Centromere] = []
    with gzip.open(path, "rt", encoding="utf-8") as fasta_file:
        for record in fasta.read(fasta_file):
            if record.name in sequences:
                candidates = [
                    k for k in get_gaps_regions(record.seq) if k[1] - k[0] in gap_sizes
                ]
                if len(candidates) == 1:
                    list.append(
                        result, (record.name, candidates[0][0], candidates[0][1])
                    )
    return result


def get_gaps_regions(sequence: str) -> List[Region]:
    """Get regions of gaps in a nucleotide sequence."""

    Acc = Tuple[List[Region], int]

    def process_contig(acc: Acc, new: contig.Contig) -> Acc:
        """Process a new contig."""
        if new.gap_length == 0:
            return acc
        gaps, current_position = acc
        gap_start = current_position + len(new.seq)
        gap_end = gap_start + new.gap_length
        list.append(gaps, (gap_start, gap_end))
        return (gaps, gap_end)

    init: Acc = ([], 0)
    result, _ = functools.reduce(process_contig, contig.to_contigs(sequence), init)
    return result


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Get assembled sequence information for an NCBI assembly."
    )
    parser.add_argument("gff3", help="NCBI GFF3 file of assembly annotation")
    parser.add_argument("report", help="NCBI assembly report")
    parser.add_argument("-f", "--fasta", help="assembly FASTA file")
    parser.add_argument(
        "-c",
        "--centromere-gap-sizes",
        type=int,
        default=[2_000_000],
        nargs="+",
        help="sizes of gaps that may represent centromeres",
    )
    return parser.parse_args(args)


def get_ncbi_assembly_info(args: argparse.Namespace) -> None:
    """Main function of the script."""
    seq_info = get_sequence_info(args.gff3, args.report)
    centromeres_from_gff3 = get_centromere_info(args.gff3)
    if not centromeres_from_gff3 and args.fasta is not None:
        centromeres = {
            accn: (start, end)
            for accn, start, end in get_centromeres_from_fasta(
                args.fasta,
                {k.accession for k in seq_info if k.seq_type == "assembled-molecule"},
                args.centromere_gap_sizes,
            )
        }
    else:
        centromeres = {accn: (start, end) for accn, start, end in centromeres_from_gff3}

    writer = csv.writer(sys.stdout)
    writer.writerow(["Accession", "Chromosome", "Type", "Length", "Centromere"])
    for k in seq_info:
        writer.writerow(
            [
                k[0],
                "NA" if k[2] == "Unknown" else k[2],
                k[1],
                k[3],
                str.format("{:d}-{:d}", centromeres[k[0]][0], centromeres[k[0]][1])
                if k[0] in centromeres
                else "NA",
            ]
        )


if __name__ == "__main__":
    get_ncbi_assembly_info(parse_args())
