"""Get the VCF file of variants for a FASTA file with ambiguous IUPAC symbols."""

import sys
from typing import List, Tuple

from pygenomics import fasta, iupac
from pygenomics.vcf import header
from pygenomics.vcf.data import alt, record
from pygenomics.vcf.meta import contig, filterline, regular


def process_ambiguous_nucleotide(
    symbol: str,
) -> Tuple[record.RefField, List[alt.Field], record.FilterField]:
    """Get REF, ALT and FILTER values for an ambiguous nucleotide VCF data line."""
    parsed_symbol = iupac.Symbol.of_string(symbol)
    assert parsed_symbol is not None
    nucleotides = iupac.Symbol.to_nucleotides(parsed_symbol)
    assert len(nucleotides) in {2, 3}
    return (
        record.RefField(str(nucleotides[0])),
        [alt.Field(alt.FieldType.BASE_PAIRS, str(k)) for k in nucleotides[1:]],
        record.FilterField([] if len(nucleotides) == 2 else ["triplet"]),
    )


def process_record(
    seq_record: fasta.Record,
) -> Tuple[contig.ContigLine, List[record.Record]]:
    """Process a FASTA record.

    :param seq_record: Nucleotide sequence record from a FASTA file.

    :return: VCF contig meta-information line and the list of records.

    """
    variant_lines: List[record.Record] = []
    for k, nucleotide in enumerate(seq_record.seq, start=1):
        if nucleotide in iupac.get_ambiguous_symbol_characters():
            ref_field, alt_field, filter_field = process_ambiguous_nucleotide(
                nucleotide
            )
            list.append(
                variant_lines,
                record.Record(
                    record.Chrom(seq_record.name, False),
                    record.Pos(k),
                    record.IdField([]),
                    ref_field,
                    alt_field,
                    record.Qual(None),
                    filter_field,
                    record.InfoField([]),
                    "",
                ),
            )
    return (contig.ContigLine(seq_record.name, len(seq_record.seq), []), variant_lines)


def process_fasta_file(path: str) -> None:
    """Print the VCF file of ambiguous nucleotide positions."""
    with open(path, encoding="utf-8") as fasta_file:
        results = [process_record(k) for k in fasta.read(fasta_file)]
    print(regular.RegularLine("fileformat", "VCFv4.3"))
    print(regular.RegularLine("reference", str.format("file://{:s}", path)))
    print(
        filterline.FilterLine(
            "triplet",
            [("Description", "Ambiguous IUPAC code encoding three nucleotides")],
        )
    )
    for contig_line, _ in results:
        print(contig_line)
    print(header.Line([]))
    for _, contig_variants in results:
        for variant_line in contig_variants:
            print(variant_line)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 get_iupac_fasta_vcf.py sequences.fa", file=sys.stderr)
        sys.exit(1)
    process_fasta_file(sys.argv[1])
