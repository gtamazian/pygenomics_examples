"""Get lengths of reference sequences from the VCF file header."""

import gzip
import sys

import pygenomics.vcf.reader


def print_ref_lengths(vcf_path: str) -> None:
    """Print reference sequence lengths from the specified VCF file."""
    with gzip.open(vcf_path, "rt", encoding="utf-8") as vcf_file:
        reader = pygenomics.vcf.reader.Reader(vcf_file)
        for k in reader.meta.contigs:
            print(str.format("{:s}\t{:d}", k.name, k.length))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 get_vcf_ref_lengths.py variants.vcf.gz", file=sys.stderr)
        sys.exit(1)
    print_ref_lengths(sys.argv[1])
