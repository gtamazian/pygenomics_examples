#!/usr/bin/env python3

"""Extract the gene annotation from BUSCO-Metaeuk results."""

import argparse
import functools
import itertools
import operator
import os
import os.path
from typing import Callable, Dict, List, NamedTuple, Optional, Set, Tuple

from pygenomics import fasta, nucleotide
from pygenomics.gff import base, gff3
from pygenomics.strand import Strand
from pygenomics_ext.busco import fulltable

_START_CODON = "ATG"
_STOP_CODONS = {"TAG", "TAA", "TGA"}

_REV_START_CODON = nucleotide.rev_comp(_START_CODON)
_REV_STOP_CODONS = {nucleotide.rev_comp(k) for k in _STOP_CODONS}


def is_start_codon(codon: str, is_reverse: bool) -> bool:
    """Check if the specified codon is a start codon."""
    return str.upper(codon) == (_START_CODON if not is_reverse else _REV_START_CODON)


def is_stop_codon(codon: str, is_reverse: bool) -> bool:
    """Check if the specified codon is a stop codon."""
    return str.upper(codon) in (_STOP_CODONS if not is_reverse else _REV_STOP_CODONS)


def read_fasta(path: str) -> List[fasta.Record]:
    """Read records from a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        return list(fasta.read(fasta_file))


class GeneFiles(NamedTuple):
    """Names of files for predicted genes, transcripts, and proteins."""

    gff_path: str
    cds_path: str
    pep_path: str


class BuscoFiles(NamedTuple):
    """Names of files with BUSCO-Metaeuk results."""

    table_path: str
    rerun_path: str
    initial: GeneFiles
    rerun: GeneFiles


def is_rerun_file(path: str) -> bool:
    """Does the path correspond to the file of rerun sequences?"""
    return path == "refseq_db_rerun.faa"


def is_gff_file(path: str) -> bool:
    """Does the path correspond to a gene GFF3 file?"""
    return str.endswith(path, ".gff")


def is_cds_file(path: str) -> bool:
    """Does the path correspond to a CDS file?"""
    return str.endswith(path, ".codon.fas")


def is_pep_file(path: str) -> bool:
    """Does the path correspond to a protein file?"""
    return str.endswith(path, ".modified.fas")


def look_for_gene_files(root: str, paths: List[str]) -> List[GeneFiles]:
    """Identify complete gene file sets."""
    gff_paths = [k for k in paths if is_gff_file(k)]
    cds_paths = [k for k in paths if is_cds_file(k)]
    pep_paths = [k for k in paths if is_pep_file(k)]
    if len(gff_paths) == len(cds_paths) == len(pep_paths) == 1:
        return [
            GeneFiles(
                os.path.join(root, gff_paths[0]),
                os.path.join(root, cds_paths[0]),
                os.path.join(root, pep_paths[0]),
            )
        ]
    return []


class BuscoFilesNotFound(Exception):
    """Indicates that the complete BUSCO gene set could not be found."""

    path: str

    def __init__(self, path: str):
        super().__init__(path)
        self.path = path


def get_busco_file_paths(table_path: str) -> BuscoFiles:
    """Get BUSCO-Metaeuk file paths from the full table path."""
    base_dir = os.path.split(table_path)[0]
    rerun_paths: List[str] = []
    initial_candidates: List[GeneFiles] = []
    rerun_candidates: List[GeneFiles] = []

    for root, _, files in os.walk(base_dir):
        rerun_paths += [os.path.join(root, k) for k in files if is_rerun_file(k)]
        if str.endswith(root, "initial_results"):
            initial_candidates += look_for_gene_files(root, files)
        elif str.endswith(root, "rerun_results"):
            rerun_candidates += look_for_gene_files(root, files)

    if not len(rerun_paths) == len(initial_candidates) == len(rerun_candidates) == 1:
        raise BuscoFilesNotFound(base_dir)

    return BuscoFiles(
        table_path, rerun_paths[0], initial_candidates[0], rerun_candidates[0]
    )


Interval = Tuple[str, int, int]


class BuscoGeneInfo(NamedTuple):
    """Annotated BUSCO gene status and position."""

    busco_id: str
    status: fulltable.GeneStatus
    position: Interval


def read_busco_gene_info(path: str) -> List[BuscoGeneInfo]:
    """Read BUSCO gene information from a full table file."""
    return [
        BuscoGeneInfo(
            k.busco_id, k.status, (k.gene.scaffold, k.gene.start + 1, k.gene.end + 1)
        )
        for k in fulltable.read(path)
        if k.gene is not None
    ]


def get_busco_gene_positions(info: List[BuscoGeneInfo]) -> Dict[str, Set[Interval]]:
    """Get the dictionary of BUSCO gene positions."""
    return {
        j: set(k.position for k in gene_info)
        for j, gene_info in itertools.groupby(
            sorted(info, key=operator.attrgetter("busco_id")),
            key=operator.attrgetter("busco_id"),
        )
    }


def get_busco_gene_statuses(info: List[BuscoGeneInfo]) -> Dict[str, str]:
    """Get the dictionary of BUSCO gene statuses."""
    values = {(k.busco_id, str(k.status)) for k in info}
    assert len(set(k for k, _ in values)) == len(values)
    return dict(values)


class IncorrectGff3Line(Exception):
    """Indicates an incorrect line in the GFF3 file."""

    path: str
    line: str

    def __init__(self, path: str, line: str):
        super().__init__(path, line)
        self.path = path
        self.line = line


def is_gff3_exon_line(line: str) -> bool:
    """Does the line correspond to an exon feature?"""
    parts = str.split(line, "\t", 2)
    return parts[1] == "exon" if len(parts) > 1 else False


def fix_gff3_line(line: str) -> str:
    """Fix CDS coordinates in a GFF3 line."""
    parts = str.split(line, "\t")
    start, end = int(parts[3]), int(parts[4])
    return str.join(
        "\t",
        [
            parts[0],
            parts[2],
            parts[1],
            str.format("{:d}", end if start > end else start),
            str.format("{:d}", end),
        ]
        + parts[5:],
    )


def read_gff3_file(path: str) -> List[gff3.Record]:
    """Read records from a Metaeuk GFF3 file."""
    result: List[gff3.Record] = []
    with open(path, encoding="utf-8") as gff3_file:
        for line in gff3_file:
            if is_gff3_exon_line(line):
                continue
            current = gff3.Record.of_string(str.rstrip(fix_gff3_line(line)))
            if current is None:
                raise IncorrectGff3Line(path, line)
            list.append(result, current)
    return result


def group_gff3_records(records: List[gff3.Record]) -> List[List[gff3.Record]]:
    """Group GFF3 records by genes."""

    Acc = Tuple[List[List[gff3.Record]], List[gff3.Record]]

    def process_record(acc: Acc, new: gff3.Record) -> Acc:
        """Process a new GFF3 record."""
        processed, current = acc
        if new.pos.feature_type == "gene":
            list.append(processed, current)
            current = [new]
        else:
            list.append(current, new)

        return (processed, current)

    init: Acc = ([], [records[0]])
    genes, last = functools.reduce(process_record, records[1:], init)
    list.append(genes, last)
    return genes


def remove_suffix(busco_id: str) -> str:
    """Remove the number suffix from a BUSCO ID."""
    return str.split(busco_id, "_", 1)[0]


def get_gff3_gene_position(records: List[gff3.Record]) -> Interval:
    """Get the position of the gene record."""
    return (
        records[0].pos.seqid,
        records[0].pos.record_range.start,
        records[0].pos.record_range.end,
    )


def get_busco_id(records: List[gff3.Record]) -> str:
    """Get the BUSCO ID for the gene record."""
    return remove_suffix(dict(records[0].attributes.pairs)["Target_ID"])


class IntervalNameError(Exception):
    """Incorrect interval name in the BUSCO-Metaeuk FASTA file."""

    name: str

    def __init__(self, name: str):
        super().__init__(name)
        self.name = name


def get_protein_interval(name: str) -> Interval:
    """Get the interval from a protein sequence name."""
    name_parts = str.split(name, ":", 1)
    if len(name_parts) != 2:
        raise IntervalNameError(name)
    coord_parts = str.split(name_parts[1], "-", 1)
    if len(coord_parts) != 2:
        raise IntervalNameError(name)
    try:
        start = int(coord_parts[0])
        end = int(coord_parts[1])
    except ValueError as error:
        raise IntervalNameError(name) from error
    return (name_parts[0], start + 1, end + 1)


def get_cds_interval(name: str) -> Interval:
    """Get the interval from a CDS sequence name."""
    name_parts = str.split(name, "|", 8)
    if len(name_parts) != 9:
        raise IntervalNameError(name)
    try:
        start = int(name_parts[6])
        end = int(name_parts[7])
    except ValueError as error:
        raise IntervalNameError(name) from error
    return (name_parts[1], start + 1, end + 1)


def read_sequences(
    path: str, name_fn: Callable[[str], Interval]
) -> Dict[Interval, str]:
    """Read modified protein sequences from a BUSCO-Metaeuk FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        result: Dict[Interval, str] = {}
        for name, _, seq in fasta.read(fasta_file):
            result[name_fn(name)] = seq
        return result


class GeneRecordError(Exception):
    """Indicates that the list of GFF3 records does not encode a gene."""


def is_proper_gene(records: List[gff3.Record]) -> bool:
    """Check that the list of GFF3 records encodes a proper gene."""
    return len(records) >= 3 and (
        records[0].pos.feature_type == "gene"
        and records[1].pos.feature_type == "mRNA"
        and all(k.pos.feature_type == "CDS" for k in records[2:])
    )


def get_gene_records(
    busco_files: BuscoFiles,
) -> Tuple[List[List[gff3.Record]], Dict[Interval, str], Dict[Interval, str]]:
    # pylint: disable=too-many-locals
    """Get the gene records."""
    gene_positions = get_busco_gene_positions(
        read_busco_gene_info(busco_files.table_path)
    )
    valid_positions = {j: k for k, v in dict.items(gene_positions) for j in v}
    assert len(valid_positions) == sum(len(k) for k in dict.values(gene_positions))

    rerun_pep = read_sequences(busco_files.rerun.pep_path, get_protein_interval)
    rerun_cds = read_sequences(busco_files.rerun.cds_path, get_cds_interval)

    rerun_genes = [
        k
        for k in group_gff3_records(read_gff3_file(busco_files.rerun.gff_path))
        if get_gff3_gene_position(k) in valid_positions
        and get_gff3_gene_position(k) in rerun_pep
    ]

    rerun_gene_ids = {str.split(k.name)[0] for k in read_fasta(busco_files.rerun_path)}
    rerun_gene_intervals = {get_gff3_gene_position(k) for k in rerun_genes}

    initial_pep = read_sequences(busco_files.initial.pep_path, get_protein_interval)
    initial_cds = read_sequences(busco_files.initial.cds_path, get_cds_interval)

    initial_genes = [
        k
        for k in group_gff3_records(read_gff3_file(busco_files.initial.gff_path))
        if get_gff3_gene_position(k) in valid_positions
        and get_gff3_gene_position(k) in initial_pep
        and get_gff3_gene_position(k) not in rerun_gene_intervals
        and get_busco_id(k) not in rerun_gene_ids
    ]

    initial_gene_intervals = {get_gff3_gene_position(k) for k in initial_genes}
    rerun_gene_intervals = {get_gff3_gene_position(k) for k in rerun_genes}
    assert not set.intersection(initial_gene_intervals, rerun_gene_intervals)
    gene_intervals = set.union(initial_gene_intervals, rerun_gene_intervals)

    deduplicated_genes = [
        list(k)[0]
        for _, k in itertools.groupby(
            sorted(initial_genes + rerun_genes, key=get_gff3_gene_position),
            key=get_gff3_gene_position,
        )
    ]

    gff = [k for k in deduplicated_genes if get_gff3_gene_position(k) in gene_intervals]
    cds = {
        k: v
        for k, v in list(dict.items(initial_cds)) + list(dict.items(rerun_cds))
        if k in gene_intervals
    }
    pep = {
        k: v
        for k, v in list(dict.items(initial_pep)) + list(dict.items(rerun_pep))
        if k in gene_intervals
    }

    assert len(gff) == len(cds) == len(pep)

    if not all(is_proper_gene(k) for k in gff):
        raise GeneRecordError()

    return (gff, cds, pep)


def update_gene_record(record: gff3.Record, number: int, status: str) -> gff3.Record:
    """Update gene attribute fields."""
    gene_id = str.format("gene_{:d}", number)
    annotation_id = dict(record.attributes.pairs)["Target_ID"]
    busco_id = str.split(annotation_id, "_", 1)[0]
    return gff3.Record(
        record.pos,
        gff3.Attributes(
            [
                ("ID", gene_id),
                ("BuscoID", busco_id),
                ("BuscoStatus", status),
                ("AnnotationID", annotation_id),
            ]
        ),
    )


def update_mrna_record(record: gff3.Record, gene_number: int) -> gff3.Record:
    """Update attributes of an mRNA record."""
    return gff3.Record(
        record.pos,
        gff3.Attributes(
            [
                ("ID", str.format("mRNA_{:d}", gene_number)),
                ("Parent", str.format("gene_{:d}", gene_number)),
            ]
        ),
    )


def update_cds_records(
    records: List[gff3.Record], gene_number: int
) -> List[gff3.Record]:
    """Update attributes of CDS records."""
    return [
        gff3.Record(
            k.pos,
            gff3.Attributes(
                [
                    ("ID", str.format("cds_{:d}_{:d}", gene_number, j)),
                    ("Parent", str.format("mRNA_{:d}", gene_number)),
                ]
            ),
        )
        for j, k in enumerate(sorted(records, key=lambda x: x.pos.start), start=1)
    ]


def change_coordinates(record: gff3.Record, start: int, end: int) -> gff3.Record:
    """Change coordinates of a GFF3 record."""
    return gff3.Record(
        base.Record(
            record.pos.seqid,
            record.pos.source,
            record.pos.feature_type,
            base.Range(start, end),
            record.pos.score,
            record.pos.strand,
            record.pos.phase,
        ),
        record.attributes,
    )


def add_stop_codon(
    records: List[gff3.Record], gene_cds: str, sequence: str
) -> Tuple[List[gff3.Record], str]:
    """Try to add the stop codon to the list of gene GFF3 records."""
    gene, mrna, cds = records[0], records[1], records[2:]
    if gene.pos.strand is Strand.PLUS:
        if is_stop_codon(sequence[gene.pos.end : gene.pos.end + 3], False):
            return (
                (
                    [
                        change_coordinates(gene, gene.pos.start, gene.pos.end + 3),
                        change_coordinates(mrna, mrna.pos.start, mrna.pos.end + 3),
                    ]
                    + cds[:-1]
                    + [
                        change_coordinates(
                            cds[-1], cds[-1].pos.start, cds[-1].pos.end + 3
                        ),
                        gff3.Record(
                            base.Record(
                                gene.pos.seqid,
                                gene.pos.source,
                                "stop_codon",
                                base.Range(cds[-1].pos.end + 1, cds[-1].pos.end + 3),
                                None,
                                gene.pos.strand,
                                gene.pos.phase,
                            ),
                            gff3.Attributes(cds[-1].attributes.pairs[1:]),
                        ),
                    ]
                ),
                gene_cds + sequence[gene.pos.end : gene.pos.end + 3],
            )
        return (records, gene_cds)
    if is_stop_codon(sequence[gene.pos.start - 4 : gene.pos.start - 1], True):
        return (
            [
                change_coordinates(gene, gene.pos.start - 3, gene.pos.end),
                change_coordinates(mrna, mrna.pos.start - 3, mrna.pos.end),
                gff3.Record(
                    base.Record(
                        gene.pos.seqid,
                        gene.pos.source,
                        "stop_codon",
                        base.Range(cds[0].pos.start - 3, cds[0].pos.start - 1),
                        None,
                        gene.pos.strand,
                        gene.pos.phase,
                    ),
                    gff3.Attributes(cds[0].attributes.pairs[1:]),
                ),
                change_coordinates(cds[0], cds[0].pos.start - 3, cds[0].pos.end),
            ]
            + cds[1:],
            gene_cds
            + nucleotide.rev_comp(sequence[gene.pos.start - 4 : gene.pos.start - 1]),
        )
    return (records, gene_cds)


def get_mrna_id(gene: List[gff3.Record]) -> str:
    """Get the mRNA ID for the gene."""
    assert gene[1].pos.feature_type == "mRNA"
    return dict(gene[1].attributes.pairs)["ID"]


def process_gene(
    records: List[gff3.Record],
    cds: str,
    pep: str,
    number: int,
    status: str,
    sequence: str,
) -> Tuple[List[gff3.Record], fasta.Record, fasta.Record]:
    # pylint: disable=too-many-arguments
    """Process a single gene."""
    new_gene, new_cds = add_stop_codon(
        [
            update_gene_record(records[0], number, status),
            update_mrna_record(records[1], number),
        ]
        + update_cds_records(records[2:], number),
        cds,
        sequence,
    )
    return (
        new_gene,
        fasta.Record(get_mrna_id(new_gene), "", new_cds),
        fasta.Record(get_mrna_id(new_gene), "", pep),
    )


def prepare_genes(
    busco_path: str, fasta_path: str
) -> List[Tuple[List[gff3.Record], fasta.Record, fasta.Record]]:
    """Prepare GFF3 and FASTA records of BUSCO-Metaeuk protein-coding genes."""
    busco_files = get_busco_file_paths(busco_path)
    gff, cds, pep = get_gene_records(busco_files)
    statuses = get_busco_gene_statuses(read_busco_gene_info(busco_files.table_path))
    sequences = {k.name: k.seq for k in read_fasta(fasta_path)}
    results = [
        process_gene(
            gene,
            cds[get_gff3_gene_position(gene)],
            pep[get_gff3_gene_position(gene)],
            k,
            statuses[get_busco_id(gene)],
            sequences[gene[0].pos.seqid],
        )
        for k, gene in enumerate(gff, start=1)
        if get_busco_id(gene) in statuses
    ]
    return results


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Get protein-coding gene annotation from BUSCO-Metaeuk rsults."
    )
    parser.add_argument("full_table", help="BUSCO full table file")
    parser.add_argument("reference", help="FASTA file of genome sequences")
    parser.add_argument("out_prefix", help="prefix for output files")
    return parser.parse_args(args)


def get_busco_metaeuk_genes(args: argparse.Namespace) -> None:
    """Main function of the scripts."""
    results = prepare_genes(args.full_table, args.reference)

    with open(args.out_prefix + ".gff", "w", encoding="utf-8") as gff_file:
        print("##gff-version 3", file=gff_file)
        for gene, _, _ in results:
            for record in gene:
                print(record, file=gff_file)

    with open(args.out_prefix + ".cds.fna", "w", encoding="utf-8") as cds_file:
        for _, cds, _ in results:
            fasta.write(cds_file, cds)

    with open(args.out_prefix + ".pep.faa", "w", encoding="utf-8") as pep_file:
        for _, _, pep in results:
            fasta.write(pep_file, pep)


if __name__ == "__main__":
    get_busco_metaeuk_genes(parse_args())
