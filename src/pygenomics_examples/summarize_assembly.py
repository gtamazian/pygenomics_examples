#!/usr/bin/env python3

"""Summarize scaffolds and contigs of a genome assembly."""

import argparse
import functools
from typing import List, NamedTuple, Optional

from pygenomics import contig, fasta
from pygenomics_ext.ncbi import assemblyreport

Assembly = List[fasta.Record]


def read_assembly(path: str) -> List[fasta.Record]:
    """Read sequences of a genome assembly from the specified file."""
    with open(path, encoding="utf-8") as fasta_file:
        return list(fasta.read(fasta_file))


class ContigSummary(NamedTuple):
    """Summary of contigs and gaps."""

    contig_number: int
    gap_number: int
    total_gap_length: int


def summarize_contigs(records: Assembly) -> ContigSummary:
    """Summarize contigs and gaps in a genome assembly."""

    def summarize_sequence(scaffold: str) -> ContigSummary:
        """Summarize contigs and gaps for a single scaffold."""
        scaffold_contigs = contig.to_contigs(scaffold)
        return ContigSummary(
            len(scaffold_contigs),
            sum(k.gap_length > 0 for k in scaffold_contigs),
            sum(k.gap_length for k in scaffold_contigs),
        )

    def process_sequence(acc: ContigSummary, seq: str) -> ContigSummary:
        """Add up counts from two summary records."""
        new_summary = summarize_sequence(seq)
        return ContigSummary(
            acc.contig_number + new_summary.contig_number,
            acc.gap_number + new_summary.gap_number,
            acc.total_gap_length + new_summary.total_gap_length,
        )

    return functools.reduce(
        process_sequence, (k.seq for k in records), ContigSummary(0, 0, 0)
    )


class ScaffoldSummary(NamedTuple):
    """Summary of scaffolds."""

    scaffold_number: int
    total_scaffold_length: int


def summarize_scaffolds(records: Assembly) -> ScaffoldSummary:
    """Summarize scaffolds in a genome assembly."""
    return ScaffoldSummary(len(records), sum(len(k.seq) for k in records))


def is_primary(sequence: assemblyreport.Sequence) -> bool:
    """Does the sequence belong to a primary assembly?"""
    return (
        sequence.assembly_unit.unit_type
        is assemblyreport.AssemblyUnitType.PRIMARY_ASSEMBLY
    )


def is_chromosome(sequence: assemblyreport.Sequence) -> bool:
    """Does the sequence represent a nuclear chromosome?"""
    return (
        sequence.seq_role is assemblyreport.SequenceRole.ASSEMBLED_MOLECULE
        and sequence.molecule_type is assemblyreport.MoleculeType.CHROMOSOME
        and is_primary(sequence)
    )


def is_unlocalized(sequence: assemblyreport.Sequence) -> bool:
    """Does the sequence represent an unlocalized scaffold?"""
    return (
        sequence.seq_role is assemblyreport.SequenceRole.UNLOCALIZED_SCAFFOLD
        and sequence.molecule_type is assemblyreport.MoleculeType.CHROMOSOME
        and is_primary(sequence)
    )


def is_unplaced(sequence: assemblyreport.Sequence) -> bool:
    """Does the sequence represent an unplaced scaffold?"""
    return (
        sequence.seq_role is assemblyreport.SequenceRole.UNPLACED_SCAFFOLD
        and sequence.molecule_type is None
        and is_primary(sequence)
    )


class NonChromosomalScaffolds(NamedTuple):
    """Counts of unlocalized and unplaced scaffolds."""

    unlocalized_number: int
    unplaced_number: int


def summarize_nonchromosomal_scaffolds(
    report: List[assemblyreport.Sequence],
) -> NonChromosomalScaffolds:
    """Count unlocalized and unplaced scaffolds."""
    return NonChromosomalScaffolds(
        sum(is_unlocalized(k) for k in report), sum(is_unplaced(k) for k in report)
    )


def get_chromosome_percentage_from_report(
    report: List[assemblyreport.Sequence],
) -> float:
    """Get the percentage of assembled chromosomes using a report."""
    total = sum(k.seq_length for k in report if is_primary(k))
    chromosomes = sum(k.seq_length for k in report if is_chromosome(k))
    return chromosomes / total * 100


def get_chromosome_percentage_from_sequences(
    records: Assembly, num_chromosomes: int
) -> float:
    """Get the percentage of assembled chromosome using sequences."""
    seq_lengths = sorted((len(k.seq) for k in records), reverse=True)
    return sum(seq_lengths[:num_chromosomes]) / sum(seq_lengths) * 100


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line options of the script."""
    parser = argparse.ArgumentParser(description="Summarize a genome assembly.")
    parser.add_argument("fasta", help="FASTA file of assembly")
    parser.add_argument("-r", "--report", help="NCBI Assembly report")
    parser.add_argument(
        "-c",
        "--chromosomes",
        type=int,
        help="number of chromosomes in assembled genome",
    )
    return parser.parse_args(args)


def summarize_assembly(args: argparse.Namespace) -> None:
    """Main function of the script."""
    assembly = read_assembly(args.fasta)
    scaffold_summary = summarize_scaffolds(assembly)
    contig_summary = summarize_contigs(assembly)
    print(str.format("# File: {:s}", args.fasta))
    print(str.format("Total length\t{:d}", scaffold_summary.total_scaffold_length))
    print(str.format("Number of scaffolds\t{:d}", scaffold_summary.scaffold_number))
    print(str.format("Number of contigs\t{:d}", contig_summary.contig_number))
    print(str.format("Number of gaps\t{:d}", contig_summary.gap_number))
    print(str.format("Total length of gaps\t{:d}", contig_summary.total_gap_length))
    if args.chromosomes is not None:
        print(
            str.format(
                "Assembly in chromosomes\t{:.2f}%",
                get_chromosome_percentage_from_sequences(assembly, args.chromosomes),
            )
        )
    elif args.report is not None:
        report = assemblyreport.read(args.report)
        nonchromosome_scaffold_summary = summarize_nonchromosomal_scaffolds(report)
        print(
            str.format(
                "Assembly in chromosomes\t{:.2f}%",
                get_chromosome_percentage_from_report(report),
            )
        )
        print(
            str.format(
                "Number of unlocalized scaffolds\t{:d}",
                nonchromosome_scaffold_summary.unlocalized_number,
            )
        )
        print(
            str.format(
                "Number of unplaced scaffolds\t{:d}",
                nonchromosome_scaffold_summary.unplaced_number,
            )
        )


if __name__ == "__main__":
    summarize_assembly(parse_args())
