"""Parse a GFF3 file of genes annotated by Augustus.

The script creates a GFF3 file of annotated protein-coding genes, a
FASTA file of the gene coding DNA sequences (CDS), and a FASTA file of
the predicted protein sequences. The script requires the reference
genome FASTA file for building the CDS sequences. Names of the output
files of the script start with the prefix specified as one of the
script command-line parameters. A user can specify another GFF3 file
of genes: Augustus genes that overlap the specified genes will be
excluded from output.

"""

import argparse
import functools
import itertools
from typing import IO, Dict, Iterator, List, NamedTuple, Optional, Tuple

from pygenomics import common, fasta, nucleotide, strand
from pygenomics.gff import gff3, gtf
from pygenomics.interval import GenomicBase

from pygenomics_examples.get_gff3_feature_regions import get_gene_regions


def is_gene_start(line: str) -> bool:
    """Does the line designate the beginning of a gene?"""
    return str.startswith(line, "# start gene")


def is_gene_end(line: str) -> bool:
    """Does the line designate the end of a gene?"""
    return str.startswith(line, "# end gene")


def read_block(path: str) -> Iterator[List[str]]:
    """Iterate annotated gene blocks from an Augustus GFF3 file."""

    with open(path, encoding="utf-8") as gff3_file:
        # skip the header
        try:
            line = next(gff3_file)
        except StopIteration:
            return

        for line in gff3_file:
            if is_gene_start(line):
                break

        block: List[str] = [str.rstrip(line)]
        for line in map(str.rstrip, gff3_file):
            list.append(block, line)
            if is_gene_end(line):
                yield block
                block = []

        assert not block


class TranscriptRecord(NamedTuple):
    """Augustus transcript record in the GTF format."""

    gff3_records: List[gff3.Record]
    protein_sequence: str
    evidence: List[str]


class GeneRecord(NamedTuple):
    """Augustus gene record in the GTF format."""

    gene_record: gff3.Record
    transcripts: List[TranscriptRecord]


_META_LINE_START = "# "
_PROTEIN_PREFIX = "# protein sequence = ["
_PROTEIN_SUFFIX = "]"


def verify_block(block: List[str]) -> None:
    """Check that the block has proper start and end lines."""
    assert is_gene_start(block[0]) and is_gene_end(block[-1])
    start_gene_name = block[0][len("# start gene ") :]
    end_gene_name = block[-1][len("# end gene ") :]
    assert start_gene_name == end_gene_name


def fix_gene_line(line: str) -> Tuple[str, str]:
    """Fix the gene line from an Augustus GFF file."""
    gene_id = str.rsplit(line, "\t", 1)[1]
    return (
        str.format('{:s}\tgene_id "{:s}";', str.rsplit(line, "\t", 1)[0], gene_id),
        gene_id,
    )


def split_into_transcript_blocks(
    block: List[str],
) -> Tuple[str, gtf.Record, List[List[str]]]:
    """Split a gene block into transcript blocks."""
    verify_block(block)
    stripped_blocks = block[1:-1]
    assert len(stripped_blocks) >= 2
    gene_line, gene_id = fix_gene_line(stripped_blocks[0])
    gene_record = gtf.Record.of_string(gene_line)
    assert gene_record is not None

    Acc = Tuple[List[List[str]], List[str], int]

    def process_line(acc: Acc, new_line: str) -> Acc:
        """Process a new line from the gene block."""
        processed, current, state = acc
        # state = 1 corresponds to the first part of the
        # transcript block (that is, GFF lines); state = 2
        # corresponds to the second part of the transcript block
        # (that is, the protein sequence and transcript evidence
        # part)
        if state == 1:
            if str.startswith(new_line, "#"):
                state = 2
            list.append(current, new_line)
        else:
            assert state == 2
            if not str.startswith(new_line, "#"):
                state = 1
                list.append(processed, current)
                current = [new_line]
            else:
                list.append(current, new_line)
        return (processed, current, state)

    init: Acc = ([], [stripped_blocks[1]], 1)
    transcripts, last_transcript, last_status = functools.reduce(
        process_line, stripped_blocks[2:], init
    )
    assert last_status == 2
    list.append(transcripts, last_transcript)
    return (gene_id, gene_record, transcripts)


def get_protein_sequence(block: List[str]) -> str:
    """Get the transcript protein sequence."""

    Acc = Tuple[List[List[str]], List[str]]

    def is_protein_start_line(line: str) -> bool:
        """Does the line contain the protein sequence start?"""
        return str.startswith(line, "# protein sequence = [")

    def is_protein_end_line(line: str) -> bool:
        """Does the line contain the protein sequence end?"""
        return str.endswith(line, "]")

    def process_line(acc: Acc, new_line: str) -> Acc:
        """Process a line to find the protein sequence record."""
        processed, current = acc
        if current:
            list.append(current, new_line)
            if is_protein_end_line(new_line):
                list.append(processed, current)
                current = []
        else:
            if is_protein_start_line(new_line):
                assert not current
                if is_protein_end_line(new_line):
                    list.append(processed, [new_line])
                else:
                    list.append(current, new_line)
        return (processed, current)

    def concat_protein_lines(lines: List[str]) -> str:
        """Concatenate protein lines into the protein sequence."""
        assert (
            str.startswith(lines[0], _PROTEIN_PREFIX)
            and str.endswith(lines[-1], _PROTEIN_SUFFIX)
            and all(str.startswith(k, _META_LINE_START) for k in lines)
        )
        if len(lines) == 1:
            return lines[0][len(_PROTEIN_PREFIX) : -len(_PROTEIN_SUFFIX)]
        lines[0] = lines[0][len(_PROTEIN_PREFIX) :]
        lines[-1] = lines[-1][: -len(_PROTEIN_SUFFIX)]
        lines[1:] = [k[len(_META_LINE_START) :] for k in lines[1:]]
        return str.join("", lines)

    init: Acc = ([], [])
    protein_lines, _ = functools.reduce(process_line, block, init)
    assert len(protein_lines) == 1
    return concat_protein_lines(protein_lines[0])


def fix_transcript_line(line: str, gene_id: str) -> str:
    """Fix the transcript line from an Augustus GFF file."""
    transcript_id = str.rsplit(line, "\t", 1)[1]
    return str.format(
        '{:s}\ttranscript_id "{:s}"; gene_id "{:s}";',
        str.rsplit(line, "\t", 1)[0],
        transcript_id,
        gene_id,
    )


def parse_transcript_gff_lines(block: List[str], gene_id: str) -> List[gtf.Record]:
    """Extract and parse Augustus GFF3 lines."""
    gff_lines = [k for k in block if not str.startswith(k, "#")]
    gff_lines[0] = fix_transcript_line(gff_lines[0], gene_id)
    raw_records = [gtf.Record.of_string(k) for k in gff_lines]
    filtered_records = [k for k in raw_records if k is not None]
    assert len(filtered_records) == len(raw_records)
    return filtered_records


def gene_gtf_to_gff3(record: gtf.Record) -> gff3.Record:
    """Convert the gene GTF record to the GFF3 format."""
    attributes = dict(record.attributes.pairs)
    return gff3.Record(record.pos, gff3.Attributes([("ID", attributes["gene_id"])]))


def process_block(block: List[gtf.Record]) -> List[gff3.Record]:
    """Process a block of records that correspond to a transcript."""
    attributes = dict(block[0].attributes.pairs)
    transcript_record = gff3.Record(
        block[0].pos,
        gff3.Attributes(
            [("ID", attributes["transcript_id"]), ("Parent", attributes["gene_id"])]
        ),
    )
    cds_records = [
        gff3.Record(
            k.pos,
            gff3.Attributes(
                [
                    (
                        "ID",
                        str.format("cds_{:d}_{:s}", j, attributes["transcript_id"]),
                    ),
                    ("Parent", attributes["transcript_id"]),
                ]
            ),
        )
        for j, k in enumerate(block[1:], start=1)
    ]
    return [transcript_record] + cds_records


def parse_block(block: List[str]) -> GeneRecord:
    """Parse a gene and its transcripts."""
    gene_id, gene_record, transcript_blocks = split_into_transcript_blocks(block)
    transcripts = [parse_transcript_gff_lines(k, gene_id) for k in transcript_blocks]
    protein_sequences = [get_protein_sequence(k) for k in transcript_blocks]
    evidences = [
        list(itertools.dropwhile(lambda x: not str.startswith(x, "# Evidence for"), k))
        for k in transcript_blocks
    ]

    return GeneRecord(
        gene_gtf_to_gff3(gene_record),
        [
            TranscriptRecord(process_block(j), k, l)
            for j, k, l in zip(transcripts, protein_sequences, evidences)
        ],
    )


_SUPPORT_PERCENTAGE_PREFIX = "# % of transcript supported by hints (any source): "


def get_evidence_support(transcript: TranscriptRecord) -> Optional[float]:
    """Get the percentage of transcript support by hints."""
    percentage_lines = [
        k for k in transcript.evidence if str.startswith(k, _SUPPORT_PERCENTAGE_PREFIX)
    ]
    if len(percentage_lines) != 1:
        return None
    percentage_str = percentage_lines[0][len(_SUPPORT_PERCENTAGE_PREFIX) :]
    try:
        return float(percentage_str)
    except ValueError:
        return None


def get_gene_evidence_support(gene: GeneRecord) -> Optional[float]:
    """Get the maximum support percentage for transcripts of a gene."""
    percentages = [get_evidence_support(k) for k in gene.transcripts]
    present_values = [k for k in percentages if k is not None]
    return max(present_values) if present_values else None


def load_ref_sequences(path: str) -> Dict[str, str]:
    """Get the dictionary of reference genome sequences.

    The dictionary keys are sequence names and its values are the sequences.

    """
    with open(path, encoding="utf-8") as fasta_file:
        return {record.name: record.seq for record in fasta.read(fasta_file)}


def extract_cds(record: TranscriptRecord, reference: Dict[str, str]) -> str:
    """Extract CDS from reference sequences."""
    seqid = list(set(k.pos.seqid for k in record.gff3_records))
    transcript_strand = list(set(k.pos.strand for k in record.gff3_records))
    assert len(seqid) == len(transcript_strand) == 1
    ref_sequence = reference[seqid[0]]
    cds = str.join(
        "",
        (
            ref_sequence[(k.pos.start - 1) : k.pos.end]
            for k in record.gff3_records
            if k.pos.feature_type == "CDS"
        ),
    )
    return (
        nucleotide.rev_comp(cds) if transcript_strand[0] == strand.Strand.MINUS else cds
    )


def add_evince_support_percentage(gene: GeneRecord) -> GeneRecord:
    """Add the evidence support percentage as a GTF attribute."""
    new_gene = gff3.Record(
        gene.gene_record.pos,
        gff3.Attributes(
            gene.gene_record.attributes.pairs
            + [("EvidencePerc", str.format("{:.2f}", get_gene_evidence_support(gene)))]
        ),
    )
    return GeneRecord(new_gene, gene.transcripts)


def write_gene(stream: IO[str], gene: GeneRecord) -> None:
    """Write the gene record in the GTF format to the specified stream."""
    print(gene.gene_record, file=stream)
    for k in gene.transcripts:
        for cds in k.gff3_records:
            print(cds, file=stream)


def get_transcript_id(record: TranscriptRecord) -> str:
    """Get transcript ID."""
    value, _ = common.assoc_extract(record.gff3_records[0].attributes.pairs, "ID")
    return value[0]


def get_gene_cds_sequences(
    gene: GeneRecord, reference: Dict[str, str]
) -> List[fasta.Record]:
    """Get FASTA records of gene CDSs."""
    return [
        fasta.Record(get_transcript_id(k), "", extract_cds(k, reference))
        for k in gene.transcripts
    ]


def get_gene_protein_sequences(gene: GeneRecord) -> List[fasta.Record]:
    """Get FASTA records of gene proteins."""
    return [
        fasta.Record(get_transcript_id(k), "", k.protein_sequence)
        for k in gene.transcripts
    ]


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Parse an Augustus GFF gene annotation file."
    )
    parser.add_argument("gff_file", help="Augustus GFF file")
    parser.add_argument("fasta_file", help="reference genome FASTA file")
    parser.add_argument("out_prefix", help="prefix for output files")
    parser.add_argument("--excluded", help="list of excluded scaffolds")
    return parser.parse_args(args)


def load_excluded_regions(path: str) -> GenomicBase:
    """Load excluded gene regions from a GFF3 file."""
    return GenomicBase(
        (k.pos.seqid, k.pos.record_range.start, k.pos.record_range.end)
        for k in get_gene_regions(path)
    )


def get_gene_interval(gene: GeneRecord) -> Tuple[str, int, int]:
    """Get gene sequence name."""
    return (
        gene.gene_record.pos.seqid,
        gene.gene_record.pos.record_range.start,
        gene.gene_record.pos.record_range.end,
    )


def process_augustus_output(args: argparse.Namespace) -> None:
    """Main function of the script."""
    gene_blocks = [parse_block(k) for k in read_block(args.gff_file)]
    ref_dict = load_ref_sequences(args.fasta_file)
    if args.excluded:
        excluded_regions = load_excluded_regions(args.excluded)
        gene_blocks = [
            k
            for k in gene_blocks
            if not GenomicBase.find(excluded_regions, get_gene_interval(k))
        ]

    gene_blocks = [add_evince_support_percentage(k) for k in gene_blocks]

    with open(args.out_prefix + "_genes.gff", "w", encoding="utf-8") as gff_file:
        print("##gff-version 3", file=gff_file)
        for gene in gene_blocks:
            write_gene(gff_file, gene)

    with open(args.out_prefix + "_cds.fa", "w", encoding="utf-8") as cds_file:
        for cds in itertools.chain.from_iterable(
            get_gene_cds_sequences(k, ref_dict) for k in gene_blocks
        ):
            fasta.write(cds_file, cds)

    with open(args.out_prefix + "_pep.fa", "w", encoding="utf-8") as pep_file:
        for pep in itertools.chain.from_iterable(
            get_gene_protein_sequences(k) for k in gene_blocks
        ):
            fasta.write(pep_file, pep)


if __name__ == "__main__":
    process_augustus_output(parse_args())
