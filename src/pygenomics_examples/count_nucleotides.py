#!/usr/bin/env python3

"""Print nucleotide counts for each sequence in a FASTA file."""

import collections
import sys
from typing import Counter

from pygenomics import fasta


def print_nucleotide_counts(name: str, counts: Counter[str]) -> None:
    """Print nucleotide counts in the tabular form."""
    others = sum(v for k, v in dict.items(counts) if k not in set("ACGTN"))
    print(
        str.format(
            "{:s}\t{:d}\t{:d}\t{:d}\t{:d}\t{:d}\t{:d}",
            name,
            counts["A"],
            counts["C"],
            counts["G"],
            counts["T"],
            counts["N"],
            others,
        )
    )


def count_nucleotides(path: str) -> None:
    """Detect records with gap sequences and report their names and lengths."""
    with open(path, encoding="utf-8") as fasta_file:
        print("# " + str.join("\t", ["Name", "A", "C", "G", "T", "N", "Others"]))
        for record in fasta.read(fasta_file):
            print_nucleotide_counts(
                record.name, collections.Counter(str.upper(record.seq))
            )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 count_nucleotides.py sequences.fa", file=sys.stderr)
        sys.exit(1)
    count_nucleotides(sys.argv[1])
