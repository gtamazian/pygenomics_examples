#!/usr/bin/env python3

"""Lift over intervals from a GFF3 file to a Juicebox assembly."""

import argparse
import itertools
from typing import List, Optional, Tuple

from pygenomics.cli.interval.common import iterate_gff3_records
from pygenomics.gff import base, gff3
from pygenomics.strand import Strand

from pygenomics_examples.extract_ncbi_coding_genes import iterate_grouped_records
from pygenomics_examples.liftover_juicebox_bed import (
    AssemblyMapping,
    get_assembly_map,
    invert_mapped_intervals,
)

Interval = Tuple[str, int, int]


def get_bed_interval(record: gff3.Record) -> Interval:
    """Get the BED interval for a GFF3 record."""
    return (
        record.pos.seqid,
        record.pos.record_range.start - 1,
        record.pos.record_range.end,
    )


def update_interval(record: gff3.Record, new: Tuple[Interval, bool]) -> gff3.Record:
    """Update the interval for a GFF3 record."""
    (seqid, start, end), is_reverse = new
    return gff3.Record(
        base.Record(
            seqid,
            record.pos.source,
            record.pos.feature_type,
            base.Range(start + 1, end),
            record.pos.score,
            Strand.invert(record.pos.strand) if is_reverse else record.pos.strand,
            record.pos.phase,
        ),
        record.attributes,
    )


def map_records(
    mapping: AssemblyMapping, records: List[gff3.Record]
) -> Optional[List[gff3.Record]]:
    """Map a series of GFF3 records to a Juicebox assembly."""
    mapped_intervals = [
        AssemblyMapping.map_interval(mapping, get_bed_interval(k)) for k in records
    ]
    filtered_intervals = [k for k in mapped_intervals if k is not None]
    if len(filtered_intervals) < len(mapped_intervals):
        # some intervals could not be mapped
        return None
    return [update_interval(j, k) for j, k in zip(records, filtered_intervals)]


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Lift over a GFF3 file to a Juicebox assembly."
    )
    parser.add_argument("assembly", help="Juicebox .assembly file")
    parser.add_argument("gff3", help="GFF3 file to be lifted over")
    parser.add_argument(
        "-f", "--failed", help="write lift over failures to the specified file"
    )
    parser.add_argument(
        "-i", "--invert", action="store_true", help="invert the interval mapping"
    )
    return parser.parse_args(args)


def liftover_juicebox_gff3(args: argparse.Namespace) -> None:
    """Main function of the script."""
    mapped_intervals = get_assembly_map(args.assembly)
    if args.invert:
        mapped_intervals = invert_mapped_intervals(mapped_intervals)
    mapping = AssemblyMapping(mapped_intervals)
    successes: List[List[gff3.Record]] = []
    failures: List[List[gff3.Record]] = []
    for k in iterate_grouped_records(iterate_gff3_records(args.gff3)):
        mapped_records = map_records(mapping, k)
        if mapped_records is None:
            list.append(failures, k)
        else:
            list.append(successes, mapped_records)

    for k in sorted(
        successes, key=lambda x: (x[0].pos.seqid, x[0].pos.record_range.start)
    ):
        for j in k:
            print(j)

    if args.failed is not None:
        with open(args.failed, "wt", encoding="utf-8") as failures_file:
            for record in itertools.chain.from_iterable(failures):
                print(record, file=failures_file)


if __name__ == "__main__":
    liftover_juicebox_gff3(parse_args())
