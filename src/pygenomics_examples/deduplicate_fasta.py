#!/usr/bin/env python3

"""Deduplicate sequences from a FASTA file."""

import argparse
import collections
import itertools
import operator
from typing import Iterable, List, Tuple

from pygenomics import fasta, nucleotide


def normalize_record(record: fasta.Record) -> Tuple[fasta.Record, bool]:
    """Normalize a FASTA record and report the normalization result."""
    rev_comp_seq = nucleotide.rev_comp(record.seq)
    if rev_comp_seq < record.seq:
        return (fasta.Record(record.name, record.comment, rev_comp_seq), True)
    return (record, False)


class DuplicateNames(Exception):
    """Indicate duplicate record names in a FASTA file."""

    names: List[str]

    def __init__(self, names: List[str]):
        self.names = names


def get_duplicates(names: Iterable[str]) -> List[str]:
    """Get names that occur more than once."""
    return [k for k, count in dict.items(collections.Counter(names)) if count > 1]


def read_normalized_records(path: str) -> List[Tuple[fasta.Record, bool]]:
    """Read and normalize sequence records from a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        records = [normalize_record(k) for k in fasta.read(fasta_file)]
    duplicate_names = get_duplicates(k.name for k, _ in records)
    if duplicate_names:
        raise DuplicateNames(duplicate_names)
    return records


def rev_comp_rec(record: fasta.Record) -> fasta.Record:
    """Change the sequence of a FASTA record to its reverse complement."""
    return fasta.Record(record.name, record.comment, nucleotide.rev_comp(record.seq))


def deduplicate_normalized(
    records: List[Tuple[fasta.Record, bool]]
) -> List[fasta.Record]:
    """Get restored FASTA records with unique sequences."""
    seqs = (k for k, _ in records)
    is_rev_comp = {j.name: k for j, k in records}
    selected = [
        list(k)[0]
        for _, k in itertools.groupby(
            sorted(seqs, key=operator.attrgetter("seq")), key=operator.attrgetter("seq")
        )
    ]
    return [rev_comp_rec(k) if is_rev_comp[k.name] else k for k in selected]


def read_records(path: str) -> List[fasta.Record]:
    """Read sequence records from a FASTA file."""
    with open(path, encoding="utf-8") as fasta_file:
        records = list(fasta.read(fasta_file))
    duplicate_names = get_duplicates(k.name for k in records)
    if duplicate_names:
        raise DuplicateNames(duplicate_names)
    return records


def deduplicate(records: List[fasta.Record]) -> List[fasta.Record]:
    """Keep FASTA records with unique sequences."""
    return [
        list(k)[0]
        for _, k in itertools.groupby(
            sorted(records, key=operator.attrgetter("seq")),
            key=operator.attrgetter("seq"),
        )
    ]


def write_fasta(path: str, records: List[fasta.Record]) -> None:
    """Write FASTA sequences to the specified file."""
    with open(path, "w", encoding="utf-8") as fasta_file:
        for k in records:
            fasta.write(fasta_file, k)


def main(args: argparse.Namespace) -> None:
    """The main function of the script."""
    write_fasta(
        args.out_fasta,
        deduplicate_normalized(read_normalized_records(args.in_fasta))
        if args.with_rc
        else deduplicate(read_records(args.in_fasta)),
    )


def parse_args() -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Remove duplicate sequences from a FASTA file."
    )
    parser.add_argument("in_fasta", help="input FASTA file")
    parser.add_argument("out_fasta", help="output FASTA file")
    parser.add_argument(
        "--with-rc",
        action="store_true",
        help="Consider sequences equal to their reverse complements",
    )
    return parser.parse_args()


if __name__ == "__main__":
    main(parse_args())
