#!/usr/bin/env python3

"""Annotate BUSCO-derived genes with information from OrthoDB."""

import argparse
import gzip
import os.path
from enum import Enum, auto
from typing import Dict, List, NamedTuple, Optional, Tuple

from pygenomics.common import assoc_single
from pygenomics.gff import gff3


class OrthoDBErrorType(Enum):
    """Types of errors related to OrthoDB."""

    MISSING_SPECIES = auto()
    MISSING_LEVEL = auto()
    INCONSISTENT_LEVEL = auto()


class OrthoDBError(Exception):
    """Indicates an error related to OrthoDB."""

    error_type: OrthoDBErrorType
    path: str
    content: str

    def __init__(self, error_type: OrthoDBErrorType, path: str, species: str):
        super().__init__(error_type, path, species)
        self.error_type = error_type
        self.path = path
        self.species = species


def get_species_id(orthodb_root: str, species: str) -> str:
    """Get the OrthoDB ID for the species."""
    species_lowercase = str.lower(species)
    path = os.path.join(orthodb_root, "odb10v1_species.tab.gz")
    with gzip.open(path, "rt", encoding="utf-8") as species_file:
        for line in species_file:
            parts = str.split(str.rstrip(line), "\t", 3)
            if str.lower(parts[2]) == species_lowercase:
                return parts[1]
        raise OrthoDBError(OrthoDBErrorType.MISSING_SPECIES, path, species)


def get_level_id(orthodb_root: str, level: str) -> str:
    """Get the OrthoDB ID for the level."""
    level_lowercase = str.lower(level)
    path = os.path.join(orthodb_root, "odb10v1_levels.tab.gz")
    with gzip.open(path, "rt", encoding="utf-8") as level_file:
        for line in level_file:
            parts = str.split(str.rstrip(line), "\t", 4)
            if str.lower(parts[1]) == level_lowercase:
                return parts[0]
        raise OrthoDBError(OrthoDBErrorType.MISSING_LEVEL, path, level)


def get_species_levels(orthodb_root: str, species_id: str) -> List[str]:
    """Get OrthoDB levels the species is included in."""
    path = os.path.join(orthodb_root, "odb10v1_level2species.tab.gz")
    with gzip.open(path, "rt", encoding="utf-8") as level_file:
        for line in level_file:
            parts = str.split(str.rstrip(line), "\t", 3)
            if parts[1] == species_id:
                return str.split(parts[3][1:-1], ",")
        raise OrthoDBError(OrthoDBErrorType.INCONSISTENT_LEVEL, path, species_id)


def load_orthodb_gene_ids(
    orthodb_root: str, level_id: str, species_id: str
) -> List[Tuple[str, str]]:
    """Return pairs of OrthoDB orthologous group IDs and OrthoDB gene ID."""
    path = os.path.join(orthodb_root, "odb10v1_OG2genes.tab.gz")
    genes: List[Tuple[str, str]] = []

    def parse_line(line: str) -> Tuple[str, str, str, str]:
        """Parse a line from file 'odbv10_OG2genes.tab.gz'."""
        unique_id, orthodb_id = str.split(str.rstrip(line), "\t", 1)
        level_part = str.split(unique_id, "at", 1)[1]
        species_part = str.split(orthodb_id, ":", 1)[0]
        return (unique_id, orthodb_id, level_part, species_part)

    with gzip.open(path, "rt") as og_file:
        for line in og_file:
            input_id, gene_id, level, species = parse_line(line)
            if level == level_id and species == species_id:
                list.append(genes, (input_id, gene_id))

    return genes


class Gene(NamedTuple):
    """Gene record as in file `odb10v0_genes.tab.gz` of the OrthoDB dump."""

    orthodb_id: str
    organism_tax_id: str
    protein_seq_id: str
    ensembl_gene_name: Optional[str]
    uniprot_id: Optional[str]
    ensembl_gene_id: Optional[str]
    description: Optional[str]


def load_genes(orthodb_root: str, species_id: str) -> List[Gene]:
    """Load OrthoDB gene records for the given species."""

    def optional(value: str) -> Optional[str]:
        """Parse a missing value."""
        return value if value != "" else None

    OptStr = Optional[str]

    def parse_ids(line_parts: List[str]) -> Tuple[OptStr, OptStr, OptStr]:
        """Parse optional ID fields of a line."""
        parsed = [optional(k) for k in line_parts]
        if len(parsed) < 3:
            parsed += [None] * (3 - len(parsed))
        return (parsed[0], parsed[1], parsed[2])

    def parse_line(line: str) -> Gene:
        """Parse a line from file `odb10v0_genes.tab.gz`."""
        line_parts = str.split(str.rstrip(line), "\t", 6)

        id_part = parse_ids(line_parts[3:6])
        description = None if len(line_parts) < 7 else line_parts[6]
        return Gene(line_parts[0], line_parts[1], line_parts[2], *id_part, description)

    def check_species(record: Gene) -> bool:
        """Check if the gene record belongs to the species."""
        return str.split(record.orthodb_id, ":", 1)[0] == species_id

    genes: List[Gene] = []

    path = os.path.join(orthodb_root, "odb10v1_genes.tab.gz")
    with gzip.open(path, "rt", encoding="utf-8") as gene_file:
        for line in gene_file:
            record = parse_line(line)
            if check_species(record):
                list.append(genes, record)

    return genes


def build_gene_dictionary(
    orthodb_root: str, species_id: str, level_id: str
) -> Dict[str, Gene]:
    """Build the dictionary of OrthoDB gene annotations."""
    _ = get_species_levels(orthodb_root, species_id)
    gene_annotations = {k.orthodb_id: k for k in load_genes(orthodb_root, species_id)}
    return {
        input_id: gene_annotations[orthodb_id]
        for input_id, orthodb_id in load_orthodb_gene_ids(
            orthodb_root, level_id, species_id
        )
        if orthodb_id in gene_annotations
    }


def annotate_record(annotations: Dict[str, Gene], record: gff3.Record) -> gff3.Record:
    """Add gene annotation to the GFF3 record."""
    if record.pos.feature_type != "gene":
        return record
    busco_id = assoc_single(record.attributes.pairs, "BuscoID")
    if busco_id is None or busco_id not in annotations:
        return record
    gene = annotations[busco_id]
    return gff3.Record(
        record.pos,
        gff3.Attributes(
            record.attributes.pairs
            + (
                [("Name", gene.ensembl_gene_name)]
                if gene.ensembl_gene_name is not None
                else []
            )
            + (
                [("EnsemblID", gene.ensembl_gene_id)]
                if gene.ensembl_gene_id is not None
                else []
            )
            + ([("UniprotID", gene.uniprot_id)] if gene.uniprot_id is not None else [])
            + (
                [("Description", gene.description)]
                if gene.description is not None
                else []
            )
        ),
    )


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Add OrthoDB information to BUSCO-annotated genes."
    )
    parser.add_argument("orthodb_root", help="root directory of the OrthoDB dump")
    parser.add_argument("species", help="species name (e.g., 'Felis catus')")
    parser.add_argument("level", help="annotation level name (e.g., 'Carnivora')")
    parser.add_argument("gff3_file", help="GFF3 file with BUSCO-annotated genes")
    return parser.parse_args(args)


class Gff3ReadingError(Exception):
    """Error while parsing a GFF3 record."""

    path: str
    line: str

    def __init__(self, path: str, line: str):
        super().__init__(path, line)
        self.path = path
        self.line = line


def annotate_busco_genes(args: argparse.Namespace) -> None:
    """Main function of the script."""
    annotation = build_gene_dictionary(
        args.orthodb_root,
        get_species_id(args.orthodb_root, args.species),
        get_level_id(args.orthodb_root, args.level),
    )
    with open(args.gff3_file, encoding="utf-8") as gff3_file:
        for line in map(str.rstrip, gff3_file):
            if str.startswith(line, "#"):
                print(line)
            else:
                record = gff3.Record.of_string(line)
                if record is None:
                    raise Gff3ReadingError(args.gff3_file, line)
                print(annotate_record(annotation, record))


if __name__ == "__main__":
    annotate_busco_genes(parse_args())
