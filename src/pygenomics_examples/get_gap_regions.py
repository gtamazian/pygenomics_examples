#!/usr/bin/env python3

"""Get the BED file of gaps in sequences of a FASTA file."""

import functools
import sys
from typing import List, Tuple

from pygenomics import bed, contig, fasta


def get_sequence_gap_regions(sequence: fasta.Record) -> List[bed.Record]:
    """Get BED records for gaps in the sequence."""

    Acc = Tuple[List[bed.Record], int]

    def process_contig(acc: Acc, new_contig: contig.Contig) -> Acc:
        """Add the gap range from a new contig."""
        if new_contig.gap_length == 0:
            return acc
        gap_records, current_position = acc
        gap_start = current_position + len(new_contig.seq)
        gap_end = gap_start + new_contig.gap_length
        list.append(
            gap_records,
            bed.Record(bed.Bed3(sequence.name, bed.Range(gap_start, gap_end))),
        )
        return (gap_records, gap_end)

    init: Acc = ([], 0)
    result, _ = functools.reduce(process_contig, contig.to_contigs(sequence.seq), init)
    return result


def get_gap_regions(fasta_path: str) -> None:
    """Print gap regions for sequences from a FASTA file."""
    with open(fasta_path, encoding="utf-8") as fasta_file:
        for record in fasta.read(fasta_file):
            for gap in get_sequence_gap_regions(record):
                print(gap)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 get_gap_regions.py sequences.fa", file=sys.stderr)
        sys.exit(1)
    get_gap_regions(sys.argv[1])
