#!/usr/bin/env python3

"""Summarize predicted gene effects of biallelic variants."""

import collections
import gzip
import sys
from enum import Enum, auto
from typing import List, Optional, Tuple

from pygenomics.vcf.data.genotype import GenotypeRecord
from pygenomics.vcf.data.record import GenotypeFields, Record
from pygenomics.vcf.reader import Reader as VcfReader
from pygenomics_ext import snpeff


class Genotype(Enum):
    """Heterozygous or homozygous genotypes."""

    HETEROZYGOUS = auto()
    HOMOZYGOUS = auto()

    def __str__(self) -> str:
        if self is Genotype.HETEROZYGOUS:
            return "heterozygous"
        assert self is Genotype.HOMOZYGOUS
        return "homozygous"


class VariantClass(Enum):
    """Class of a genome variant based on its annotated effect."""

    SYNONYMOUS = auto()
    OTHER_LOW = auto()
    MISSENSE = auto()
    OTHER_MODERATE = auto()
    HIGH = auto()

    def __str__(self) -> str:
        if self is VariantClass.SYNONYMOUS:
            return "synonymous"
        if self is VariantClass.OTHER_LOW:
            return "low impact"
        if self is VariantClass.MISSENSE:
            return "missense"
        if self is VariantClass.OTHER_MODERATE:
            return "moderate impact"
        assert self is VariantClass.HIGH
        return "deleterious"


def convert_effect(effect: snpeff.Effect) -> VariantClass:
    """Convert a single effect to the variant class value."""
    if len(effect.effect) > 1:
        if effect.impact is snpeff.ImpactLevel.LOW:
            return VariantClass.OTHER_LOW
        if effect.impact is snpeff.ImpactLevel.MODERATE:
            return VariantClass.OTHER_MODERATE
        assert effect.impact is snpeff.ImpactLevel.HIGH
        return VariantClass.HIGH
    if effect.impact is snpeff.ImpactLevel.LOW:
        return (
            VariantClass.SYNONYMOUS
            if effect.effect[0] == "synonymous_variant"
            else VariantClass.OTHER_LOW
        )
    if effect.impact is snpeff.ImpactLevel.MODERATE:
        return (
            VariantClass.MISSENSE
            if effect.effect[0] == "missense_variant"
            else VariantClass.OTHER_MODERATE
        )
    assert effect.impact is snpeff.ImpactLevel.HIGH
    return VariantClass.HIGH


class GenotypeError(Exception):
    """Indicates that the genotype part of a variant record could not be parsed."""

    record: Record

    def __init__(self, record: Record):
        super().__init__(record)
        self.record = record


def get_genotypes(record: Record) -> List[Optional[GenotypeRecord]]:
    """Get parsed genotype records for each genotyped sample."""
    parsed_genotype_line = GenotypeFields.of_string(record.genotype_line)
    if parsed_genotype_line is None:
        raise GenotypeError(record)
    format_field, samples = parsed_genotype_line
    if format_field is None:
        raise GenotypeError(record)
    if format_field.parts and format_field.parts[0] == "GT":
        genotypes: List[Optional[GenotypeRecord]] = []
        for k in samples:
            if k.parts[0]:
                parsed_value = GenotypeRecord.of_string(k.parts[0][0])
                if parsed_value is None:
                    raise GenotypeError(record)
                list.append(genotypes, parsed_value)
            else:
                list.append(genotypes, None)
        return genotypes
    return []


class MultiallelicVariant(Exception):
    """Indicates a multiallelic variant, that the script does not support."""

    record: Record

    def __init__(self, record: Record):
        super().__init__(record)
        self.record = record


def classify_variant(
    record: Record, num_samples: int
) -> List[Optional[Tuple[VariantClass, Genotype]]]:
    """Get variant classes based on predicted effects for each allele."""
    effects = snpeff.get_effect_annotations(record)
    if not effects:
        return [None] * num_samples

    if len(record.alt) > 1:
        raise MultiallelicVariant(record)

    if len(effects) > 1:
        classes = {convert_effect(k) for k in effects}
        if len(classes) > 1:
            return [None] * num_samples
        allele_class = list(classes)[0]
    else:
        allele_class = convert_effect(effects[0])

    def get_genotype_effects(
        genotype: Optional[GenotypeRecord],
    ) -> Optional[Tuple[VariantClass, Genotype]]:
        """Get the list of effects for alleles of a single genotype."""
        if (
            genotype is None
            or genotype.missing
            or GenotypeRecord.is_reference(genotype)
        ):
            return None
        return (
            allele_class,
            Genotype.HETEROZYGOUS
            if GenotypeRecord.is_heterozygous(genotype)
            else Genotype.HOMOZYGOUS,
        )

    return [get_genotype_effects(k) for k in get_genotypes(record)]


def summarize_effects(path: str) -> None:
    """The main function of the script."""
    with gzip.open(path, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        n_samples = len(reader.samples)
        variant_classes = [classify_variant(k, n_samples) for k in reader.data]
        for k, sample in enumerate(reader.samples):
            counts = {
                k: v
                for k, v in dict.items(
                    collections.Counter(classes[k] for classes in variant_classes)
                )
                if k is not None
            }
            for (effect_type, genotype), value in dict.items(counts):
                print(
                    str.format(
                        "{:s}\t{}\t{}\t{:d}", sample, effect_type, genotype, value
                    )
                )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 summarize_effects.py variants.vcf.gz", file=sys.stderr)
        sys.exit(1)
    summarize_effects(sys.argv[1])
