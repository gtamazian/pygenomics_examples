#!/usr/bin/env python3

"""Keep records from specified scaffolds only."""

import gzip
import itertools
import sys
from typing import List, Set

from pygenomics.vcf.header import Line as VcfHeaderLine
from pygenomics.vcf.meta.metaheader import MetaHeader
from pygenomics.vcf.reader import Reader as VcfReader


def read_scaffold_list(path: str) -> List[str]:
    """Read the list of scaffolds from a file."""
    with open(path, encoding="utf-8") as scaffold_file:
        return [str.rstrip(k) for k in scaffold_file]


def print_header(meta: MetaHeader, scaffolds: Set[str]) -> None:
    """Print meta-information lines filtering them for the scaffolds."""
    for k in itertools.chain(meta.regular, meta.structured):
        print(k)
    for k in meta.contigs:
        if k.name in scaffolds:
            print(k)
    for k in itertools.chain(
        meta.info_lines,
        meta.format_lines,
        meta.alt,
        meta.filter_lines,
        meta.pedigree,
        meta.meta_lines,
        meta.sample_lines,
    ):
        print(k)


def filter_vcf_records_by_seq(vcf_path: str, scaffold_path: str) -> None:
    """Main function of the script."""
    scaffolds = set(read_scaffold_list(scaffold_path))
    with gzip.open(vcf_path, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        print_header(reader.meta, scaffolds)
        print(VcfHeaderLine(reader.samples))
        for record in reader.data:
            if record.chrom.name in scaffolds:
                print(record)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python3 filter_vcf_records_by_seq.py variants.vcf.gz scaffolds.txt",
            file=sys.stderr,
        )
        sys.exit(1)
    filter_vcf_records_by_seq(sys.argv[1], sys.argv[2])
