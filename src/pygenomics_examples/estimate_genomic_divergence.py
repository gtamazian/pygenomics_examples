#!/usr/bin/env python3

"""Estimate genome-wide divergence based on SNPs."""

import argparse
import collections
import functools
import gzip
import itertools
from typing import Dict, List, NamedTuple, Optional, Tuple

from pygenomics.interval import GenomicBase
from pygenomics.vcf.data import alt
from pygenomics.vcf.data.genotype import GenotypeRecord
from pygenomics.vcf.data.record import Record
from pygenomics.vcf.reader import Reader as VcfReader

from pygenomics_examples.summarize_effects import get_genotypes

Interval = Tuple[str, int, int]


def genotype_distance(
    first: Optional[GenotypeRecord], second: Optional[GenotypeRecord]
) -> int:
    """Calculate the number of different alleles between the genotypes.

    Zero is returned for missing genotypes or genotypes of different
    ploidy.

    """
    if first is None or second is None:
        return 0

    if first.missing or second.missing:
        return 0

    if first.ploidy != second.ploidy:
        return 0

    assert first.alleles is not None and second.alleles is not None
    return len(set.symmetric_difference(set(first.alleles), set(second.alleles)))


def is_snp(record: Record) -> bool:
    """Check that a variant is an SNP."""

    def check_alt_allele(allele: alt.Field) -> bool:
        """Check that an alternative allele corresponds to an SNP."""
        if allele.value_type is not alt.FieldType.BASE_PAIRS:
            return False
        assert allele.content is not None
        return len(allele.content) == 1

    return len(record.ref.bases) == 1 and all(check_alt_allele(k) for k in record.alt)


def process_variant(record: Record) -> Tuple[Interval, List[int]]:
    """Get pairwise distances between samples for a given variant."""
    return (
        (record.chrom.name, record.pos.value - 1, record.pos.value),
        [
            genotype_distance(j, k)
            for j, k in itertools.combinations(get_genotypes(record), 2)
        ],
    )


class GenotypeDistances(NamedTuple):
    """Distances based on genotypes."""

    base: GenomicBase
    values: Dict[Interval, List[int]]
    num_values: int

    def total_distances(self, interval: Interval) -> List[int]:
        """Count the total distances for a given interval."""
        return functools.reduce(
            lambda x, y: [j + k for j, k in zip(x, y)],
            (self.values[k] for k in GenomicBase.find_all(self.base, interval)),
            [0] * self.num_values,
        )


def read_snps(path: str) -> GenotypeDistances:
    """Read SNPs from a VCF file and return their intervals and distances."""
    with gzip.open(path, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        num_values = len(reader.samples) * len(reader.samples) // 2
        distances = dict(process_variant(k) for k in reader.data if is_snp(k))
        genomic_base = GenomicBase(dict.keys(distances))

        return GenotypeDistances(genomic_base, distances, num_values)


def get_windows(reader: VcfReader, window_size: int) -> List[Interval]:
    """Get intervals of non-overlapping windows in VCF file sequences."""

    def get_sequence_windows(name: str, length: Optional[int]) -> List[Interval]:
        """Get windows for a single sequence."""
        if length is None or length < window_size:
            return []
        return [
            (name, k, k + window_size - 1)
            for k in range(0, length, window_size)
            if k + window_size < length
        ]

    return list(
        itertools.chain.from_iterable(
            get_sequence_windows(k.name, k.length) for k in reader.meta.contigs
        )
    )


def count_differences(path: str, window_size: int) -> None:
    """Count differences for a single window."""
    variants = read_snps(path)
    with gzip.open(path, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        windows = get_windows(reader, window_size)
        sample_pairs = list(itertools.combinations(reader.samples, 2))

    total = [GenotypeDistances.total_distances(variants, k) for k in windows]
    for k, pair in enumerate(sample_pairs):
        for value, count in dict.items(collections.Counter(j[k] for j in total)):
            print(str.format("{:s}-{:s}\t{:d}\t{:d}", pair[0], pair[1], value, count))


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Estimate genome-wide divergence based on SNPs."
    )
    parser.add_argument("vcf_file", help="VCF file of genomic variants")
    parser.add_argument(
        "window_size", type=int, help="size of non-overlapping genomic windows"
    )
    return parser.parse_args(args)


def main(args: argparse.Namespace) -> None:
    """Main function of the script."""
    count_differences(args.vcf_file, args.window_size)


if __name__ == "__main__":
    main(parse_args())
