#!/usr/bin/env python3

"""Swap RefSeq or GenBank sequence accessions."""

import argparse
from typing import Dict, List, Optional, Tuple

from pygenomics.cli.interval.common import iterate_gff3_records
from pygenomics.gff import base, gff3
from pygenomics_ext.ncbi import assemblyreport


def get_mt_sequences(path: str) -> List[str]:
    """Get the mitochondrial sequence accessions."""
    return [
        k.refseq_accn
        for k in assemblyreport.read(path)
        if k.molecule_type is assemblyreport.MoleculeType.MITOCHONDRION
        and k.refseq_accn is not None
    ]


def read_seqname_pairs(path: str) -> List[Tuple[str, str]]:
    """Read pairs of sequence names and RefSeq accessions from an NCBI report."""
    return [
        (k.seq_name, k.refseq_accn)
        for k in assemblyreport.read(path)
        if k.refseq_accn is not None
    ]


def read_accessions(path: str) -> List[Tuple[str, str]]:
    """Read pairs of RefSeq and GenBank sequence accessions from an NCBI report."""
    return [
        (k.refseq_accn, k.genbank_accn)
        for k in assemblyreport.read(path)
        if k.refseq_accn is not None and k.genbank_accn is not None
    ]


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments."""
    parser = argparse.ArgumentParser(
        description="Swap between RefSeq and GenBank accessions."
    )
    parser.add_argument("report", help="NCBI assembly report")
    parser.add_argument("gff3", help="GFF3 file")
    parser.add_argument(
        "direction",
        choices=["refseq2genbank", "genbank2refseq", "orig2refseq", "refseq2orig"],
    )
    return parser.parse_args(args)


def update_seqid(record: gff3.Record, new_seqid: str) -> gff3.Record:
    """Update the sequence name for a GFF3 record."""
    return gff3.Record(
        base.Record(
            new_seqid,
            record.pos.source,
            record.pos.feature_type,
            record.pos.record_range,
            record.pos.score,
            record.pos.strand,
            record.pos.phase,
        ),
        record.attributes,
    )


def get_accession_dict(path: str, mode: str) -> Dict[str, str]:
    """Get the dictionary of old and new sequence accessions."""
    if mode == "refseq2genbank":
        return dict(read_accessions(path))
    if mode == "genbank2refseq":
        return dict((k, j) for j, k in read_accessions(path))
    if mode == "orig2refseq":
        return dict(read_seqname_pairs(path))
    assert mode == "refseq2orig"
    return dict((k, j) for j, k in read_seqname_pairs(path))


def swap_ncbidb(args: argparse.Namespace) -> None:
    """Swap RefSeq and GenBank accessions."""
    accession_dict = get_accession_dict(args.report, args.direction)
    mt_accessions = set(get_mt_sequences(args.report))
    for record in iterate_gff3_records(args.gff3):
        if record.pos.seqid not in mt_accessions:
            print(update_seqid(record, accession_dict[record.pos.seqid]))


if __name__ == "__main__":
    swap_ncbidb(parse_args())
