"""Get values of specified tags from a BAM file."""

import argparse
import gzip
import struct
from typing import List, Optional

from pygenomics.bam import datafield
from pygenomics.bam.alignment import Alignment
from pygenomics.bam.reader import Reader
from pygenomics.sam import datafield as sam_datafield


def int8_to_str(data: bytes) -> str:
    """Convert a signed 8-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<b", data)[0])


def uint8_to_str(data: bytes) -> str:
    """Convert an unsigned 8-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<B", data)[0])


def int16_to_str(data: bytes) -> str:
    """Convert a signed 16-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<h", data)[0])


def uint16_to_str(data: bytes) -> str:
    """Convert an unsigned 16-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<H", data)[0])


def int32_to_str(data: bytes) -> str:
    """Convert a signed 32-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<i", data)[0])


def uint32_to_str(data: bytes) -> str:
    """Convert an unsigned 32-bit integer to a string."""
    return str.format("{:d}", struct.unpack("<I", data)[0])


def float_to_str(data: bytes) -> str:
    """Convert a 32-bit floating point number to a string."""
    return str.format("{:g}", struct.unpack("<f", data)[0])


def char_to_str(data: bytes) -> str:
    """Convert a character to a string."""
    result: str = struct.unpack("<c", data)[0]
    return result


def str_single_value(data: bytes, data_type: datafield.ValueType) -> str:
    # pylint: disable=too-many-return-statements
    """Convert a single data value to a string."""
    if data_type.bam_value is datafield.BamSpecificValueType.INT8:
        return int8_to_str(data)
    if data_type.bam_value is datafield.BamSpecificValueType.UINT8:
        return uint8_to_str(data)
    if data_type.bam_value is datafield.BamSpecificValueType.INT16:
        return int16_to_str(data)
    if data_type.bam_value is datafield.BamSpecificValueType.UINT16:
        return uint16_to_str(data)
    if data_type.bam_value is datafield.BamSpecificValueType.INT32:
        return int32_to_str(data)
    if data_type.bam_value is datafield.BamSpecificValueType.UINT32:
        return uint32_to_str(data)
    if data_type.sam_value is sam_datafield.ValueType.REAL_NUMBER:
        return float_to_str(data)
    assert data_type.sam_value is sam_datafield.ValueType.CHARACTER
    return char_to_str(data)


def str_date_field_value(field: datafield.DataField) -> str:
    """Get the data field value as a string.

    :param field: Data field from a BAM file.

    :return: String representation of the data field.

    """
    if field.is_array:
        element_size = datafield.ValueType.value_size(field.value_type)
        return str.join(
            ",",
            (
                str_single_value(
                    field.data[k * element_size : (k + 1) * element_size],
                    field.value_type,
                )
                for k in range(len(field.data) // element_size)
            ),
        )
    if (
        field.value_type.sam_value is sam_datafield.ValueType.STRING
        or field.value_type.sam_value is sam_datafield.ValueType.HEXADECIMAL_ARRAY
    ):
        return bytes.decode(field.data[:-1])
    return str_single_value(field.data, field.value_type)


class DuplicateTags(Exception):
    """Indicates BAM auxiliary data fields with duplicate tags."""

    alignment: Alignment

    def __init__(self, alignment: Alignment):
        super().__init__(self, alignment)
        self.alignment = alignment


def get_alignment_tags(alignment: Alignment, tags: List[str]) -> List[str]:
    """Get strings for specified tags from a BAM alignment."""
    aux_data_dict = {k.tag: k for k in alignment.aux_data}
    if len(aux_data_dict) < len(alignment.aux_data):
        raise DuplicateTags(alignment)

    def get_tag_value(tag: str) -> str:
        """Get the tag value or "NA" if the field with the tag is missing."""
        if tag not in aux_data_dict:
            return "NA"
        return str_date_field_value(aux_data_dict[tag])

    return [get_tag_value(k) for k in tags]


def get_bam_tags(args: argparse.Namespace) -> None:
    """Main function of the script."""
    with gzip.open(args.bam_file, "rb") as bam_file:
        reader = Reader(bam_file)
        print("# " + str.join("\t", args.tags + (["length"] if args.length else [])))
        for record in reader.alignments:
            print(
                str.join(
                    "\t",
                    get_alignment_tags(record, args.tags)
                    + (
                        [str.format("{:d}", record.prefix.l_seq)] if args.length else []
                    ),
                )
            )


def parse_args(args: Optional[List[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Get values of tags from a BAM file.")
    parser.add_argument("bam_file", help="BAM file of read alignments")
    parser.add_argument(
        "tags", metavar="TAG", nargs="+", help="BAM tags to be extracted"
    )
    parser.add_argument("--length", action="store_true", help="print read length")
    return parser.parse_args(args)


if __name__ == "__main__":
    get_bam_tags(parse_args())
