#!/usr/bin/env python3

"""Get putative duplicated genome regions using BUSCO genes."""

import csv
import functools
import itertools
import sys
from typing import List, NamedTuple, Tuple

from pygenomics_ext.busco import fulltable


class IdentifiedGeneRecord(NamedTuple):
    """Record of a gene identified by BUSCO."""

    busco_id: str
    status: fulltable.GeneStatus
    gene: fulltable.Gene


def get_non_missing_genes(genes: List[fulltable.Record]) -> List[IdentifiedGeneRecord]:
    """Get records of genes that were identified by BUSCO."""
    return [
        IdentifiedGeneRecord(k.busco_id, k.status, k.gene)
        for k in genes
        if k.gene is not None
    ]


def arrange_by_scaffolds(
    records: List[IdentifiedGeneRecord],
) -> List[List[IdentifiedGeneRecord]]:
    """Group gene records by their scaffolds and sort by the gene start positions."""

    def get_scaffold(record: IdentifiedGeneRecord) -> str:
        """Get the scaffold name for a record."""
        return record.gene.scaffold

    return [
        sorted(k, key=lambda x: x.gene.start)
        for _, k in itertools.groupby(
            sorted(records, key=get_scaffold), key=get_scaffold
        )
    ]


def get_duplication_regions(
    genes: List[fulltable.Record],
) -> List[List[IdentifiedGeneRecord]]:
    """Get regions of consecutive and duplicated BUSCO genes."""

    arranged_genes = arrange_by_scaffolds(get_non_missing_genes(genes))

    Acc = Tuple[List[List[IdentifiedGeneRecord]], List[IdentifiedGeneRecord]]

    def process_record(acc: Acc, new: IdentifiedGeneRecord) -> Acc:
        """Process a new annotated gene record."""
        processed, current = acc
        return (
            (processed, current + [new])
            if new.status == fulltable.GeneStatus.DUPLICATED
            else (processed + [current], [])
        )

    def process_scaffold(
        scaffold_genes: List[IdentifiedGeneRecord],
    ) -> List[List[IdentifiedGeneRecord]]:
        """Get duplication regions for a scaffold."""
        if not scaffold_genes:
            return []

        init: Acc = (
            [],
            [scaffold_genes[0]]
            if scaffold_genes[0].status == fulltable.GeneStatus.DUPLICATED
            else [],
        )
        scaffold_processed, scaffold_last = functools.reduce(
            process_record, scaffold_genes[1:], init
        )
        return [k for k in scaffold_processed + [scaffold_last] if len(k) > 1]

    return list(
        itertools.chain.from_iterable(process_scaffold(k) for k in arranged_genes)
    )


def report_regions(busco_path: str) -> None:
    """Main function of the script."""
    regions = get_duplication_regions(fulltable.read(busco_path))
    writer = csv.writer(sys.stdout, delimiter="\t")
    writer.writerow(
        ["DR ID", "Scaffold", "Start", "End", "Busco ID", "Gene description"]
    )
    for j, series in enumerate(regions, start=1):
        tag = str.format("DR_{:d}", j)
        for k in series:
            writer.writerow(
                [
                    tag,
                    k.gene.scaffold,
                    k.gene.start,
                    k.gene.end,
                    k.busco_id,
                    k.gene.description,
                ]
            )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            "Usage: python3 get_duplication_regions.py full_table.csv", file=sys.stderr
        )
        sys.exit(1)
    report_regions(sys.argv[1])
