# Changelog

All notable changes to _pygenomics-examples_ will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2023-04-07

### Added

- Add test data files
- Add script for counting paired-end read lengths

## [0.1.1] - 2023-01-05

### Added

- Arrange scripts into a Python package using
  [_Poetry_](https://python-poetry.org) for the package management and
  [_tox_](https://tox.wiki) for testing the scripts.
- Add option `--with-rc` to scripts that process duplicated nucleotide
  sequences in FASTA files.

### Fixed

- Fix source code formatting

## [0.1.0] - 2022-06-24

### Added

- Initial release of the package

[0.1.2]: https://gitlab.com/gtamazian/pygenomics-examples/-/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/gtamazian/pygenomics-examples/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/gtamazian/pygenomics-examples/-/tags/0.1.0
